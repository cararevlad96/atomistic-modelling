# A repository of useful analysis scripts for molecular dynamics and testing of machine-learned potentials.

These include tools for: 
* Plotting: parity plots for energies and forces, volume scans curves, comparisons with experimental sulphur crystals, dimer curves, inter-intra splits parity plots, cummulative force error plots
* Generating volume scans
* Generating rattled configurations starting from a given molecular liquid
* 2 schemes for running automatic iterative MD
* Ovito scripts for high-throughput structure factor curves, cluster size histograms and radial distribution functions
* Utils (obtaining density in g/cm3 for a structure, etc.)
* An interface to the ASAP code for PCA plots

The Python scripts are available in the src/PhDScripts folder. 

Install via <pip install .> 