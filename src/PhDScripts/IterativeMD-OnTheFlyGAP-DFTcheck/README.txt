26.10.2021

Did a bit of cleaning up. Updated filenames, etc. 

Also, now that I want to fit materials, I need to fit the stresses as well. I just need to make sure that no stress is read from isolated molecules. 



02.08.2021

I put this in my gitlab repo.


To run spin polarised modify "prepareCastepFiles" and within iterateMD.py specify committee number of 15. 


You can checkpoint by specifying checkpoint = True, and also modifying the relevant variables: start_config, temperature, time_per_temperature...


The notebooks contain useful plotting scripts


09.07.2021

In the pre 09.07.2021 I did not have sigmas per structure, so all temperatures had the same sigma and the same threshold. 


06.07.2021 

Protocol different than the one from April.

We are pausing MD every time we do a DFT.


•	Create & start an iterative MD (on S8/S6 or both?) for (best of random spin committee approach) spin polarised & non spin polarised calculations, starting with a model trained on 10 initial structures (slightly perturbed S8/S6 rings).
•	As MD progresses (in steps of maybe 0.5fs), pause every 1fs/2fs/4fs/8fs steps (adaptively growing larger as we pass the tests described next). Select the last structure, run (best of 15 committee members / non polarised) DFT on it, find GAP-DFT error.
•	Based on the DFT forces set forces sigmas per atoms, energy sigma, and based on those set max force and energy difference threshold.
•	If the new structure has max force/energy over a certain threshold, stop MD, create new generation potential, re-run MD from start.
•	If below threshold, don’t add structure to the training set of future generation, but continue current MD.
•	The run (Langevin dynamics) would be from 300K to 1500K in steps of 100K, for 100ps per temperature.
•	Once it reaches 1500K without finding structures whose GAP-DFT energy difference is over the threshold, stop protocol and create final model.
