from ase.io import Trajectory
from ase.io import write
import os,glob

for file in glob.glob('InitialTrain*'):
    os.remove(file)

traj = Trajectory('26102021InitialTrainingTraj.traj')
for atoms in traj:
    if 'initial_charges' in atoms.arrays:
            del atoms.arrays['initial_charges']
    if 'config_type' in atoms.info:
            if("diss" in atoms.info['config_type']):
                     print(atoms.info['config_type'])
                     if 'stress' in atoms.calc.results:
                             print('here')
                             del atoms.calc.results['stress']
            if('isolated' in atoms.info['config_type']:
                     print(atoms.info['config_type'])
                     if 'stress' in atoms.calc.results:
                             del atoms.calc.results['stress']
    write('InitialTrain.xyz',atoms,append=True)
