from ase.md.langevin import Langevin
from quippy.potential import Potential
from ase import Atoms, units
from ase.io import write, Trajectory, read
import ase 
import os
import glob
import shutil
import numpy as np
import subprocess
import sys
from time import sleep



"""Taken from ASE tutorials."""
# Mind you Time is given in units of Å sqrt(u/eV). Thus, for example, 1 fs≈0.098 Å sqrt(u/eV). where u is the atomic mass unit.

timeStep = 0.5 #in fs
writingInterval = 50 #write to file after this many time steps
trajInterval = 50 #write to traj every this many steps

    


        
temperature = float(sys.argv[1])        
time = float(sys.argv[2]) # in ps

# print(temperature, time)
# temperature =  read_temperature()

atoms = read('structureToRunMDFrom.xyz')
# atoms.set_cell([24,24,24]) 
# atoms.center()
atoms.set_pbc(True)

potential = Potential(param_filename='../GAP.xml')
atoms.calc = potential


if not os.path.exists('./MD'):
    os.makedirs('./MD') 


with open('./MD/output.dat','a') as file, open('./MD/temperature.dat','a') as Tfile, open('./MD/totalEnergy.dat','a') as Efile, open('./MD/time.dat','a') as timeFile:



    def printenergy(atoms=atoms):  # store a reference to atoms in the definition.
        """Function to print the potential, kinetic and total energy."""
        epot = atoms.get_potential_energy() / len(atoms)
        ekin = atoms.get_kinetic_energy() / len(atoms)
        file.write('Energy per atom: Epot = %.3feV  Ekin = %.3feV (T=%3.0fK)  '
              'Etot = %.3feV' % (epot, ekin, 
                                  ekin / (1.5 * units.kB),
                                 epot + ekin))
        file.write('\n')
        Tfile.write(str(ekin / (1.5 * units.kB)) + '\n')
        Efile.write(str(epot + ekin) + '\n')
        timeFile.write(str(timeStep * writingInterval) + '\n')


    def runDyn(T=0,time=0):  #time in ps      

        # We want to run MD with constant energy using the Langevin algorithm
        # with a time step of 0.5 fs, the temperature T and the friction
        # coefficient to 0.02 atomic units.

#         print(temperature)

        dyn = Langevin(atoms, timeStep * units.fs, temperature_K=T, friction=0.02) # 1 units.fs = 0.09822694788464063

        dyn.attach(printenergy, interval=writingInterval)
        
        

        # We also want to save the positions of all atoms after every 100th time step.
        traj = Trajectory('./MD/langevinTrajectory.traj', 'a', atoms)

        dyn.attach(traj.write, interval=trajInterval)


        # Now run the dynamics
        n_steps = time * 1000 / timeStep # *1000 because time should be in picoseconds 
        print(n_steps)
        dyn.run(n_steps)


    runDyn(T=temperature,time=time)
    
    with open('ToShowMDHasFinished','w') as file:
        file.write("'I'm Finished'")
