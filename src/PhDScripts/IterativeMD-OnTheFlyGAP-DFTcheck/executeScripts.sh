#!/usr/bin/env bash
#method one. This executes script.sh in all subdirectories names config[userinput] to config[userinput]
COUNTER=$1
END=$2
while [ $COUNTER -le $END ]
do
	echo "config$COUNTER/"
	DIRECTORY="config$COUNTER/"
	(cd $DIRECTORY ; qsub scriptCastep.sh) #bash or qsub
	((COUNTER++))
done
