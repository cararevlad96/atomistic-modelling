"""
Each generation is programmed to go from 300 to 1400K for 50ps each century temperature. 

Each dynamics runs for a specific amount of time steps before stopping, doing an DFT on the last structure
and checking the GAP-DFT error. If this is large, we stop the MD, add the structure to the database and 
create a new generation. If it is small, we continue the MD from where we left off. 
"""

from ase.md.langevin import Langevin
from quippy.potential import Potential
from ase import Atoms, units
from ase.io import write, Trajectory, read
import ase 
import os
import glob
import shutil
import numpy as np
import subprocess
from time import sleep
from ase.io.castep import read_castep_castep

           
    
''' 
Method to write to an ouput file everything that's we want to print
'''
def decorator_to_file(func):
    def wrapper(*args):
        func(*args)
        output.write(str(*args))
        output.write('\n')
        output.flush()
    return wrapper


        
'''
Given a 2D numpy array computes what the sigmas should be for that configuration, based on a normalised
force_atom_sigma = max(log(1+<f>+f), min_sigma) where <f> is the average force on the config, and f is the force on an individual atom
and min_sigma is the minimum sigma we will use, 0.03
'''
def get_force_atom_sigmas(forces):
    
    average = np.average(forces)

    force_normalisation_const = max_force_sigma/np.log10(1+2*supposed_maximum_force) # twice because average + force_per_atom
    
    #apply log transform
    log_transform = lambda force_per_atom: force_normalisation_const*np.log10(1+average+np.abs(force_per_atom))
    
    #trim what's below 0.001 and 0.5 - tight sigmas vs hot liquid sigmas
    trim_upper_and_lower_ends = lambda force_atom_sigmas: np.minimum(max_force_sigma,np.maximum(min_force_sigma,force_atom_sigmas))
    
    force_atom_sigmas = log_transform(forces)
    force_atom_sigmas = trim_upper_and_lower_ends(force_atom_sigmas)

    return force_atom_sigmas

if __name__=='__main__':
    
    with open('output.txt','a') as output:

        print = decorator_to_file(print)        
        
        """
        Constants declarations
        """

        temperature = 300 
        temperature_increment = 100
        max_temperature = 1500

        max_time_per_temperature = 100 # ps per temperature
        time_interval_GAP_check = 1 # do a DFT every 100fs... this is going to take a loong time.. I may make it adaptive 
                                      # So, if the GAP passes the test once, double the time_interval_GAP_check

        maxIterations = 100 #max number of models

        start_counter = 0 # initialise at 0 to do from beginning


        
        min_force_sigma = 0.001
        max_force_sigma = 0.5 # Patrick Rowe used 0.5 as the max force sigma for liquid carbon

        supposed_maximum_force = 100 # when I rotate/distort my dimers in the training set, the last config, which is the most distorted,
                                    # has forces of order 1, up to 10. I will assume around 15 ev/A is the highest force I get.
        min_energy_sigma = 0.001
        max_energy_sigma = 0.05 # as in Patrick Rowe's liquid sigmas
        energy_normalisation_const = max_energy_sigma/max_force_sigma**2
        threshold_sigma_ratio = 3

        checkpoint = False # manual checkpointing 

      
        n_committee = 15 # for a spin polarisation calculation we have 15 random spin committee members. For non polarised we have 1. 
        
        
        
        
        if(start_counter==0):
            if not checkpoint:
                write('currentTrainingDB.xyz',read('InitialTrain.xyz',':'))
                print(f'Initialising training trajectory. \n Starting generation is gen{start_counter}. \n\n\n')

            
        if not checkpoint:

            structureToRunMDFrom = read('initial_structure.xyz')
            structureToRunMDFrom.set_pbc(True)

            # ONLY uncomment if you have isolated molecule as starting point
            # structureToRunMDFrom.set_cell([24,24,24]) 
            # structureToRunMDFrom.center()
            

        


        for overallCounter in range(start_counter,start_counter+maxIterations):
            
            
            
            if not checkpoint:
                print(f"--------------------CYCLE {overallCounter}/{maxIterations}--------------------")


                print("I'm at block 1 - train GAP.")




                write('train.xyz',read('currentTrainingDB.xyz',':'))

                check = glob.glob('./GAP.xml')
                if(len(check)==0):
                    subprocess.run('qsub scriptGAP.sh', shell=True, cwd='./')

                    while(len(check)==0):
                        print('GAP fit not yet finished (or there is no GAP job - do check), sleep 30 sec.')
                        sleep(30)
                        check = glob.glob('./GAP.xml')

                    print('GAP fit finished. \n')
                else:
                    print('GAP file already present in the directory. Will use that. \n \n')







            newPath = f'./gen{overallCounter}'
            
            
            if not checkpoint:

                print(f"I'm at block 2 - Set up MD run within {newPath} folder, starting from 300K and up to 1400K for {max_time_per_temperature}ps each. Stop at each {time_interval_GAP_check}ps (interval which grows adaptively) to check DFT-GAP error.") 

                try:
                    os.mkdir(newPath)
                except:
                    print('WARNING: Directory ' + newPath + ' already exists.')




            # Run MD only for small windows of time, then do a DFT on the last structure, and check GAP-DFT error


            
            
            if not checkpoint:
                temperature = 300 
            else:
                temperature = 300 
                    
            while(temperature <= max_temperature):
                
                


                if not checkpoint:
                    time_per_temperature = 0
                    adaptive_time_increment_counter = 0 

                else:
                    time_per_temperature = 0
                    adaptive_time_increment_counter = 0

                while(time_per_temperature <= max_time_per_temperature):
                    
                    time_increment = time_interval_GAP_check*2**adaptive_time_increment_counter
                    
                    
                    #don't let time increment go bigger than half the total time, such that we have at least 2 DFT-GAP checks per temperature
                    if(time_increment > max_time_per_temperature/2):
                        time_increment = max_time_per_temperature/2 +1
                        
                    #don't let the total time per temperature go over the max time per temperature.
                    if(time_per_temperature + time_increment > max_time_per_temperature):
                        time_increment = max_time_per_temperature - time_per_temperature
                        
                        if(time_increment < time_interval_GAP_check): #if time_per_temperature is so close to 
                                           # max_time, then break, to avoid running into infinite loops
                            break
                    

                    
                        
                    print(f' \n\n\n  We are at temperature {temperature}K, and time {time_per_temperature}ps.   \n\n\n')
                    
                    if not checkpoint: 
                        write(f'{newPath}/structureToRunMDFrom.xyz',structureToRunMDFrom)

                        shutil.copy('dynamics.py',newPath)
                        shutil.copy('scriptDyn.sh',newPath)


                        print(f' \n The next MD will run for {time_increment}ps.')

                        subprocess.run(f'qsub scriptDyn.sh {temperature} {time_increment}', shell=True, cwd=newPath)



                        #check if MD finished - the MD run creates a file called ToShowMDHasFinished.
                        check = glob.glob(f'{newPath}/ToShowMDHasFinished')
                        while(len(check)==0):
                            print('MD has not finished, sleeping for 1 min. ')
                            sleep(60)
                            check = glob.glob(f'{newPath}/ToShowMDHasFinished')


                        checkFile = check[0]   
                        #if the file has nothing written in it, MD not finished. 
                        while(os.stat(checkFile).st_size == 0):

                            print('MD has not finished, sleeping for 1 min. ')
                            sleep(60)
                        print('MD finished.')


                        # as our test above checks the end of MD by the presence of the a check file, delete check file after we're done
                        # to prepare for next iteration
                        os.remove(checkFile)  


                    md_traj = Trajectory(f'{newPath}/MD/langevinTrajectory.traj')
                    
                    if not checkpoint:

                        #RUN DFT 
                        print(f'\n MD trajectory has reached {time_per_temperature + time_increment}ps. Submit a DFT calculation of the latest structure. \n')
                        atoms_to_evaluate = Atoms(md_traj[-1]) 

                    auxPath = newPath + f'/configTemp{temperature}KTime{time_per_temperature + time_increment}ps' 

                    if not checkpoint:

                        os.makedirs(auxPath)
                        write(f'{auxPath}/structure_to_evaluate.xyz',atoms_to_evaluate)

                        print(f"\n\n I sent structure configTemp{temperature}Time{time_per_temperature + time_increment} to evaluate with DFT.")
                        shutil.copy('prepareCastepFiles.py',auxPath)
                        shutil.copy('castep_keywords.json',auxPath)
                        subprocess.run('python3 prepareCastepFiles.py', shell=True, cwd=auxPath)
                        # this will have created a committee of spin members


                        shutil.copy('executeScripts.sh',auxPath)
                        shutil.copy('scriptCastep.sh',auxPath)

                        subprocess.run('find . -type d -exec cp scriptCastep.sh {} \;', shell=True, cwd=auxPath) # copies scripts into
                        #all subfolders
                        subprocess.run(f'bash executeScripts.sh 0 {n_committee-1}', shell=True, cwd=auxPath)
                        #run config0 to config14





                    checkpoint = False
                    
                    

                    # when all committee members are finished they should either have 
                    # a castep.bib file or a castep.err file. 
                    check = glob.glob(f'{auxPath}/config*/castep.bib')
                    check.extend(glob.glob(f'{auxPath}/config*/*err'))
                    
                    print(f'\n Checking whether calculations finished in folder {auxPath}.')
                    while(len(check)<n_committee): 
                        print(f'\n Only {len(check)}/{n_committee} of all CASTEP calculations in {auxPath} have finished, sleeping for 2 minutes.')
                        sleep(2*60)

                        check = glob.glob(f'{auxPath}/config*/castep.bib')
                        check.extend(glob.glob(f'{auxPath}/config*/*err'))

                    sleep(30)
                    print(f'\n\n The calculations in {auxPath} finished. Now read the results, \n select the best committee member and compare to GAP prediction.')
                    [os.remove(file) for file in glob.glob(f'{auxPath}/config*/*cst_esp')]
                    [os.remove(file) for file in glob.glob(f'{auxPath}/config*/*usp')]
                    [os.remove(file) for file in glob.glob(f'{auxPath}/config*/*bib')]
                        
                        
                    spin_committee_traj = []
                    spin_committee_folders = glob.glob(f'{auxPath}/config*/')
                    for folder in spin_committee_folders:
                        try:
                            atoms_out = read_castep_castep(f'{folder}/castep.castep')[0]
                            if 'initial_charges' in atoms_out.arrays: del atoms_out.arrays['initial_charges']
                            spin_committee_traj.append(atoms_out)
                        except:
                            print(f'Folder {folder} failed SCF.')


                    lowest_energy = spin_committee_traj[0].get_potential_energy()
                    index_lowest_energy = 0

                    if(len(spin_committee_traj)>1):
                        for index in range(1,len(spin_committee_traj)):
                            if(spin_committee_traj[index].get_potential_energy()<lowest_energy):
                                lowest_energy = spin_committee_traj[index].get_potential_energy()
                                index_lowest_energy = index





                    print(f'\n\n Checking GAP-DFT error.')
                    over_threshold = False


                    atoms_dft = Atoms(spin_committee_traj[index_lowest_energy])
                    dft_forces = np.linalg.norm(atoms_dft.get_forces(),axis=1)
                    
                    atoms_gap = Atoms(spin_committee_traj[index_lowest_energy])
                    atoms_gap.calc = Potential(param_filename='./GAP.xml')
                    gap_forces = np.linalg.norm(atoms_gap.get_forces(),axis=1)
                    
                        
                    force_atom_sigmas = get_force_atom_sigmas(dft_forces)
                    # recall sigma is related to the expected error 
                    force_sigma_average = np.average(force_atom_sigmas)
                    
                    
                    # according to Gabor the threshold can be 3-5x bigger than the current sigma_f 
                    force_difference_threshold = threshold_sigma_ratio * force_sigma_average
                    forces_difference = np.abs(dft_forces - gap_forces)
                    forces_difference = np.amax(forces_difference)
                    print(f'\n GAP-DFT max. forces difference is {forces_difference} ev/A. ')
                    if(forces_difference > force_difference_threshold):
                        over_threshold = True
                        print(f'\n This is over the {force_difference_threshold} ev/A threshold.')
                    else:
                        print(f'\n This is below the {force_difference_threshold} ev/A threshold.')
                        
                    
                    # we are basing our energy sigma on forces. The energy sigma should be power 2 force sigma, times a constant
                    # make sure we don't go too low by imposing the min energy sigma
                    # the normalisation constant is picked such that when force atoms sigma average is 0.5, the energy sigma is 0.05.
                    energy_sigma = np.maximum(min_energy_sigma,force_sigma_average**2*energy_normalisation_const) 
                    
                    energy_difference_threshold = threshold_sigma_ratio * energy_sigma 
                    energy_difference  = np.abs((atoms_dft.get_potential_energy() - atoms_gap.get_potential_energy())/len(atoms_dft))
                    print(f'\n GAP-DFT energy difference is {energy_difference} ev/atom.')
                    if(energy_difference > energy_difference_threshold):
                        over_threshold = True
                        print(f'\n This is over the {energy_difference_threshold} eV/atom threshold.')
                    else:
                        print(f'\n This is below the {energy_difference_threshold} eV/atom threshold.')
                        
                        

                    spin_committee_traj[index_lowest_energy].info['energy'] = spin_committee_traj[index_lowest_energy].get_potential_energy()
                    spin_committee_traj[index_lowest_energy].arrays['forces'] = spin_committee_traj[index_lowest_energy].get_forces()
                    spin_committee_traj[index_lowest_energy].info['stress'] = spin_committee_traj[index_lowest_energy].get_stress()
                    print("WRITING STRESS AS WELL!! BEWARE IF YOU'RE DOING ISOLATED MOLECULES")

                    
                    #also, to be used if the structure gets written to the training set,
                    #set sigmas looser, according to temperature or other heurestics 
                    spin_committee_traj[index_lowest_energy].info['energy_sigma'] = energy_sigma
                    spin_committee_traj[index_lowest_energy].arrays['force_atom_sigma'] = force_atom_sigmas
                    spin_committee_traj[index_lowest_energy].arrays['virial_sigma'] = ?????
                    
                    write(f'{auxPath}/bestCommitteeMember.xyz',spin_committee_traj[index_lowest_energy])



                    # If energy and forces differences below threshold, then continue MD from where it left off
                    # Also increase the time interval for the GAP checks adaptively. 
                    if(over_threshold == False):
                        print(' \n\n Continuing MD, as my GAP prediction error is below threshold.')
                        
                        
                        time_per_temperature += time_increment
                        
                        adaptive_time_increment_counter += 1 
                            
                        structureToRunMDFrom = Atoms(md_traj[-1])



                    # If energy or forces differences over threshold, then start new generation potential and re-start MD

                    if(over_threshold == True):
                        spin_committee_traj[index_lowest_energy].info['metadata']=f'Structure from {auxPath}.'

                        print("\n\n Interrupting MD, as my GAP prediction error is over the threshold. \n Appending the last structure to the training database - currentTrainingDB.xyz.")
                        write('currentTrainingDB.xyz',spin_committee_traj[index_lowest_energy],append=True)

                        #BREAK OUT OF THE WHILE LOOPS
                        time_per_temperature = max_time_per_temperature + 1 
                        temperature = max_temperature + 1 
                        
                        


                        structureToRunMDFrom = read('initial_structure.xyz')
                        structureToRunMDFrom.set_pbc(True)

                        # ONLY uncomment if you have isolated molecule as starting point
                        # structureToRunMDFrom.set_cell([24,24,24]) 
                        # structureToRunMDFrom.center()
                        
                        

                        print("\n\n I'm at block 3 - Move GAP files to final generation - to clear workspace.")

                        files = glob.glob('./GAP*')
                        files.extend(glob.glob('./quip*'))
                        files.extend(glob.glob('./train.xyz'))
                        for file in files:
                            shutil.move(file, newPath)
                        print('Moving files')
                        print(files)




                temperature += temperature_increment










            print(f"\n\n--------------------CYCLE {overallCounter}/{maxIterations} FINISHED--------------------\n\n\n\n")

