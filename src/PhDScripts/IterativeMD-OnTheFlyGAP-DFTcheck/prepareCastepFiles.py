import os
from ase import Atoms, Atom
from ase.calculators.castep import Castep
from ase.io import read, write
import numpy as np
from ase.io import Trajectory
from ase.io.castep import write_castep_cell

    
atoms = read("S6structure.xyz")
# atoms.set_cell([24,24,24]) 
# atoms.center() # No reason to centre, 
atoms.set_pbc(True)
    


# give me random values for atomic spins, between 0 and 1
numberOfSpinConfigs = 15
start_config = 0


for spin_member_counter in range(start_config,start_config+numberOfSpinConfigs): 

    spins = [(np.random.uniform()*2-1) for i in range(0,len(atoms))]

    mpirun = "mpirun"
    mpirun_args = "-n 32"
    castep = "castep.mpi"
    os.environ['CASTEP_COMMAND'] = '{0} {1} {2}'.format(mpirun, mpirun_args, castep)
    calculator = Castep(directory=f'./config{spin_member_counter}',
                cut_off_energy=250,
                spin_polarized=True,
                spin_fix = 1,
                opt_strategy='speed',
                xc_functional='PBE',
                #elec_energy_tol='0.0000001',
                max_scf_cycles=100, # seems that if the SCF doesn't converge in 100 steps it's unlikely to converge in more 
                fix_occupancy=False,
                calculate_stress=False,
                finite_basis_corr='automatic',
                smearing_width='0.04',
                #bs_nextra_bands = '20', #according to json keywords file the default is 4
                #fine_grid_scale=4,
                mixing_scheme='pulay',
#                 metals_method = 'edft',
                #mix_history_length=20,
                #num_dump_cycles=0,
                #mix_spin_amp=0.03,
                kpoints_mp_spacing='0.040',
                #perc_extra_bands=200
                write_checkpoint='none'
                )


    atoms.calc = calculator   
    atoms.calc.initialize()



    atoms.set_initial_magnetic_moments(spins)
    fd = open(calculator._directory + '/castep.cell','w')
    write_castep_cell(fd,atoms,castep_cell = atoms.calc.cell,magnetic_moments='initial')
    fd.close()







