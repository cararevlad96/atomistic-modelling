#!/bin/bash -l
#$ -pe mpi 80
#$ -l h_rt=0:20:00
#$ -S /bin/bash
#$ -N CASTEP
#$ -j yes
#$ -cwd
#$ -P Gold
#$ -A UKCP_CAM_E

ulimit -s unlimited
export OMP_NUM_THREADS=1
module unload -f compilers/intel/2018/update3
module unload -f mpi/intel/2018/update3/intel
module load compilers/intel/2019/update4
module load mpi/intel/2019/update4/intel
module load castep/19.1.1/intel-2019

mpirun -n 40 castep.mpi castep
