#!/bin/bash -l
#$ -pe mpi 40
#$ -l h_rt=00:45:00
#$ -S /bin/bash
#$ -N MD
#$ -j yes
#$ -cwd
#$ -P Gold
#$ -A UKCP_CAM_E

ulimit -s unlimited
export OMP_NUM_THREADS=1

python3 dynamics.py $1 $2 #temperature and time 
