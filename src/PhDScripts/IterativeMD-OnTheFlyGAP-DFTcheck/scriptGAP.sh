#!/bin/bash -l
#$ -pe mpi 40
#$ -l h_rt=00:05:00
#$ -S /bin/bash
#$ -N GAPtest
#$ -j yes
#$ -cwd
#$ -P Gold
#$ -A UKCP_CAM_E

ulimit -s unlimited
export OMP_NUM_THREADS=1

bash runGap.sh
