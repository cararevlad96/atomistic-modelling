16.06.2021

First commit to GitLab


Give the program an initial train.xyz and validate.xyz
It will create a GAP potential based on that - it will used qsub to send gap fit job to the cluster! - Although 
really the program is short enough it can be done on login node
It will to the cluster a run MD with certain parameters specified in the dynamics method
If the molecule breaks at 300K stop the program and get one of the last structures, do DFT on it, add to the training
database and get a new potential - a new generation.
If it doesn't break at 300K go up to 1400K, at the end sample different structures, 
do DFT on them, calculate the GAP-DFT error on them, add them to the database, create
new potential - new generation, and continue


