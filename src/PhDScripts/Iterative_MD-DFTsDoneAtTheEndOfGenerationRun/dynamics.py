from ase.md.langevin import Langevin
from quippy.potential import Potential
from ase import Atoms, units
from ase.io import write, Trajectory, read
import ase 
import os
import glob
import shutil
import numpy as np
import subprocess
import sys
from time import sleep
from multiprocessing import Process,Pipe


pathToAdd = os.path.dirname(os.path.abspath(__file__))+'/../'
sys.path.insert(0, pathToAdd) # this script runs in a generation folder. So go to the parent folder for other modules - to get the global variables modules


import methods #own methods to check for breaking structures


"""Taken from ASE tutorials."""
# Mind you Time is given in units of Å sqrt(u/eV). Thus, for example, 1 fs≈0.098 Å sqrt(u/eV). where u is the atomic mass unit.

timeStep = 0.5 #in fs
writingInterval = 25 #write to file after this many time steps
trajInterval = 50 #write to traj every this many steps

    

# def read_temperature(): # the main script write temperature to a file
#     with open('settings.txt','r') as file:
#         for line in file.readlines():
#             if 'temperature' in line:
#                 return int(line.split()[0]) 

def write_time():
    with open('settings.txt','a') as file:
        file.write("\n" + str(time) + ' time - may not correspond to the run time - which could be cut short because of bond break.')
        
temperatures = list(range(300,1401,100))        
time = 100 # in ps
write_time()

# temperature =  read_temperature()
print(f'Will run MD at temperatures from 300 to 1400K for {time}ps each.')

atoms = read('structureToRunMDFrom.xyz')
atoms.set_cell([24,24,24]) 
atoms.center()
atoms.set_pbc(True)

potential = Potential(param_filename='../GAP.xml')
atoms.calc = potential

path = './MD'
os.mkdir(path)    


with open('./MD/output.dat','w') as file, open('./MD/temperature.dat','w') as Tfile, open('./MD/totalEnergy.dat','w') as Efile, open('./MD/time.dat','w') as timeFile:


    counter = 1

    def printenergy(atoms=atoms):  # store a reference to atoms in the definition.
        """Function to print the potential, kinetic and total energy."""
        epot = atoms.get_potential_energy() / len(atoms)
        ekin = atoms.get_kinetic_energy() / len(atoms)
        file.write('Energy per atom: Epot = %.3feV  Ekin = %.3feV (T=%3.0fK)  '
              'Etot = %.3feV' % (epot, ekin, 
                                  ekin / (1.5 * units.kB),
                                 epot + ekin))
        file.write('\n')
        Tfile.write(str(ekin / (1.5 * units.kB)) + '\n')
        Efile.write(str(epot + ekin) + '\n')

        global counter

        timeFile.write(str(timeStep*writingInterval*counter) + '\n')

        counter+=1
#         samplingTimes = []

    def check_if_breaks(atoms=atoms): #exit dyn if atoms are breaking into 2 separate clusters
        if methods.does_it_break(atoms):
            quit()

    def runDyn(T=0,time=0):  #time in ps      

        # We want to run MD with constant energy using the Langevin algorithm
        # with a time step of 0.5 fs, the temperature T and the friction
        # coefficient to 0.02 atomic units.


        dyn = Langevin(atoms, timeStep * units.fs, temperature_K=T, friction=0.02) # 1 units.fs = 0.09822694788464063

        dyn.attach(printenergy, interval=writingInterval)
        
        

        # We also want to save the positions of all atoms after every 100th time step.
        traj = Trajectory('./MD/langevinTrajectory.traj', 'a', atoms)

        dyn.attach(traj.write, interval=trajInterval)
        
        if(T<400):
            dyn.attach(check_if_breaks, interval=trajInterval) #this exits if system breaks into 2 separated clusters.
            #it's important it's called AFTER the trajectory attach
            #NOTE WE ONLY CARE THE SYSTEM IS STABLE S8 RING AT 300K. WE DO NOT WANT TO BIAS THE MD FURTHER - it-s normal it will break at higher temperartures.


        # Now run the dynamics
        steps = time * 1000 / timeStep 
        dyn.run(steps)


    for temperature in temperatures:
        runDyn(T=temperature,time=time)

