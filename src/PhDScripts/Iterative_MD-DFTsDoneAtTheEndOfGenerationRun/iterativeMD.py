"""
Each generation is programmed to go from 300 to 1400K for 100ps each century temperature. 
Each generation has it own DFT calculations calculation sampled every 2000 steps - 50ps. 

Only check if the structure breaks at 300K - at higher temps do not check as I do not want to bias the result.
"""

from ase.md.langevin import Langevin
from quippy.potential import Potential
from ase import Atoms, units
from ase.io import write, Trajectory, read
import ase 
import os
import glob
import shutil
import numpy as np
import subprocess
from time import sleep
from ase.io.castep import read_castep_castep



from methods import does_it_break #my own method to check for two clusters breaking apart





def write_temperature(path,temperature):
    with open(f'{path}/settings.txt','w') as file:
        file.write(str(temperature) + ' temperature')


        
        
        
        
        
if __name__=='__main__':
    

    #temperature = 300 # Now temperature is specified in dynamics module - from 300 to 1400 in steps of 100K
    

    maxIterations = 40

    start_counter = 15
        
        
    # just comment the block below if you begin from a generaiton other than 0
#     initialDB = read('InitialTrain.xyz',':')
#     trainingTraj = Trajectory('currentTrainingDB.traj','w')
#     for atoms in initialDB:
#         trainingTraj.write(atoms)
#     trainingTraj.close() 

    
    structureToRunMDFrom = read('S8Ring.cif')


    print(f'Initialising training trajectory. \n Starting generation is gen{start_counter}. \n\n\n')

    
    
    
    structureToRunMDFrom.set_cell([24,24,24]) 
    structureToRunMDFrom.center()
    structureToRunMDFrom.set_pbc(True)
    


    for overallCounter in range(start_counter,start_counter+maxIterations):
        
        print(f"--------------------CYCLE {overallCounter}/{maxIterations}--------------------")




        print("I'm at block 1 - train GAP.")



        
        trainingTraj = Trajectory('currentTrainingDB.traj')
        write('train.xyz',trainingTraj)
        
        check = glob.glob('./GAP.xml')
        if(len(check)==0):
            trainingTraj.close()
            subprocess.run('qsub scriptGAP.sh', shell=True, cwd='./')
            
            while(len(check)==0):
                print('GAP fit not yet finished (or there is no GAP job - do check), sleep 1 min.')
                sleep(60)
                check = glob.glob('./GAP.xml')
                
            print('GAP fit finished. \n')
        else:
            print('GAP file already present in the directory. Will use that. \n \n')
        




            

        newPath = f'./gen{overallCounter}'
        print(f"I'm at block 2 - Run MD within {newPath} folder, starting from 300K and up to 1400K for 100ps each (if struct doesn't break).") 

        try:
            os.mkdir(newPath)
        except:
            print('WARNING: Directory ' + newPath + ' already exists.')




        shutil.copy('./seeTraj.ipynb',newPath)

        write(f'{newPath}/structureToRunMDFrom.xyz',structureToRunMDFrom)

        shutil.copy('dynamics.py',newPath)
        shutil.copy('scriptDyn.sh',newPath)
        subprocess.run('qsub scriptDyn.sh', shell=True, cwd=newPath)


        listDFTPaths = []
        
        
        #check if MD finished - the MD run outputs a file called GAPtest.o[PIDnumber]. This file only gets written to when the MD is finished
        check = glob.glob(f'{newPath}/MD.o*')
        while(len(check)==0):
            print('MD has not started, sleeping for 1 min. ')
            sleep(60)
            check = glob.glob(f'{newPath}/MD.o*')
        if(len(check)==1):
            
            print('MD started.')
            
            checkFile = check[0]

            structure_to_sample_for_DFT = 0 # sample a MD structure each 50 ps to do the DFT of.
                                        #with timestep of 0.5fs, and a traj write every 50 steps, meaning every 25fs
                                        #means I have to sample every 2000 steps

            while(os.stat(checkFile).st_size == 0): #output file is not written to yet - meaning MD not finished 

                md_traj = Trajectory(f'{newPath}/MD/langevinTrajectory.traj')
                
                DFTs_so_far = len(listDFTPaths)
                if(int(len(md_traj)/2000)>DFTs_so_far):                    
                    #RUN DFT 
                    print(f'\n MD trajectory has reached a multiple of 2000 steps - submit a DFT calculation of the latest structure. \n')
                    structure_to_sample_for_DFT += 2000
                    atoms_to_evaluate = Atoms(md_traj[structure_to_sample_for_DFT-1]) 

                    auxPath = newPath + '/config' + str(structure_to_sample_for_DFT)
                    
                    listDFTPaths.append(auxPath)
                    
                    os.makedirs(auxPath)
                    write(f'{auxPath}/S8structure.xyz',atoms_to_evaluate)

                    print(f"\n I sent structure {structure_to_sample_for_DFT} to evaluate with DFT.")
                    shutil.copy('scriptCastep.sh',auxPath)
                    shutil.copy('runCastep.py',auxPath)
                    shutil.copy('castep_keywords.json',auxPath)

                    subprocess.run('qsub scriptCastep.sh', shell=True, cwd=auxPath)
                    
                    #check if DFT started by checking for castep.castep file 
                    check = glob.glob(f'{auxPath}/castep.castep')
                    while(len(check)==0):
                        print(f'CASTEP calculation in {auxPath} has not started, sleeping for 3 minutes, \n to not overload cluster. ')
                        sleep(3*60)
                        check = glob.glob(f'{auxPath}/castep.castep')


                    
                
                print('Not over, sleep 45s',end='-')
                sleep(45) # such low latency because I don't want to the program to exit before 


        md_traj = Trajectory(f'{newPath}/MD/langevinTrajectory.traj')
        if(int(len(md_traj)/2000)>len(listDFTPaths)):
            print(f'There should have been {len(md_traj)/2000} DFT calculations but there are only {len(listDFTPaths)}. \n That is because of the lag between reading the MD, sending DFTs, and the MD that continues running meanwhile. \n Do now the rest of the calculations.')
           
            structure_to_sample_for_DFT = (len(listDFTPaths)+1)*2000
            atoms_to_evaluate = Atoms(md_traj[structure_to_sample_for_DFT-1]) 

            auxPath = newPath + '/config' + str(structure_to_sample_for_DFT)

            listDFTPaths.append(auxPath)

            os.makedirs(auxPath)
            write(f'{auxPath}/S8structure.xyz',atoms_to_evaluate)

            print(f"\n I sent structure {structure_to_sample_for_DFT} to evaluate with DFT.")
            shutil.copy('scriptCastep.sh',auxPath)
            shutil.copy('runCastep.py',auxPath)
            shutil.copy('castep_keywords.json',auxPath)

            subprocess.run('qsub scriptCastep.sh', shell=True, cwd=auxPath)

            #check if DFT started by checking for castep.castep file 
            check = glob.glob(f'{auxPath}/castep.castep')
            while(len(check)==0):
                print(f'CASTEP calculation in {auxPath} has not started, sleeping for 3 minutes, \n to not overload cluster. ')
                sleep(3*60)
                check = glob.glob(f'{auxPath}/castep.castep')


            structure_to_sample_for_DFT += 2000


           
           
           




        print("\n\n I'm at block 3 - Move GAP files to final generation - to clear workspace.")

        files = glob.glob('./GAP*')
        files.extend(glob.glob('./quip*'))
        files.extend(glob.glob('./train.xyz'))
        for file in files:
            shutil.move(file, newPath)
        print('Moving files')
        print(files)





        traj = Trajectory(f'{newPath}/MD/langevinTrajectory.traj') 

        if(len(traj)<=4000): # i.e. if MD failed at 300K. We only test at 300K as I don't want to bias the system at larger T, where it is natural the system breaks.
                            #note the number 4000 depends on the time step,runtime and trajInterval in the dynamics.py module.
                            # currenly it is 0.5fs,100ps,50steps -> 100 000/0.5*50=4000
        
            print(" \n\n I'm at block 4 - check whether MD was stable or not - only for 300K. \n Note if the MD was not stable the process would have exited already anyways.")


            break_Flag = does_it_break(traj[-1]) # check whether my MD breaks S8 into 2 clusters - which I think is unphysicial at 300K, so stop


            if(break_Flag): #does this structure correspond to 2 clusters flying appart?

                print(f'The last structure of {newPath} was found to break into clusters.')




                traj = Trajectory(f'{newPath}/MD/langevinTrajectory.traj') #get whatever trajectory broke
                print("\n\n I'm at block 5.")    
                print(f"We take the structure 10 steps before breaking, the 10th structure from the end of {newPath}, \n and create an xyz file named 'breakingStructures' in the same path.")
                atoms_to_evaluate = Atoms(traj[-10]) # get the structure 10 steps previous to the breaking structure
                auxPath = newPath + '/breakingStructureDFT'

                os.makedirs(auxPath)
                write(f'{auxPath}/S8structure.xyz',atoms_to_evaluate)







                print("\n\n I'm at block 6 - Evaluating breaking structure with DFT.")
                shutil.copy('scriptCastep.sh',auxPath)
                shutil.copy('runCastep.py',auxPath)
                shutil.copy('castep_keywords.json',auxPath)

                subprocess.run('qsub scriptCastep.sh', shell=True, cwd=auxPath)


                print(f"Waiting for CASTEP to finish. Testing in folder {auxPath}.")
                evaluated_config = None
                while evaluated_config is None:
                    try:
                        evaluated_config = read_castep_castep(f'./{auxPath}/castep.castep')[0] # I used this as an absolute check of whether the CASTEP is finished, but an error showed me it's not good enough and I can get in an infinte loop. i.e. read_castep_castep(f'./gen{overallCounter}/CASTEP/castep.castep')[0] can work, AND return None even if the calculation is not finished.
                        evaluated_config.get_potential_energy() # This seems to be a good complementary test.
                    except:
                        print("Castep calculation not finished or still queueing, sleep 15 mins.")
                        sleep(15*60)#sleep 15 mins
                        pass

                evaluated_config.info['metadata']=f'Structure from {newPath}.'

                print("\n\n I'm at block 7 - Appending structures to database - currentTrainingDB.traj .")
                trainingTraj = Trajectory('currentTrainingDB.traj','a')
                trainingTraj.write(evaluated_config)
                trainingTraj.close()
        traj.close()
        
        
        trainingTraj = Trajectory('currentTrainingDB.traj','a')

        for auxPath in listDFTPaths:
            try:
                evaluated_config = read_castep_castep(f'./{auxPath}/castep.castep')[0] # I used this as an absolute check of whether the CASTEP is finished, but an error showed me it's not good enough and I can get in an infinte loop. i.e. read_castep_castep(f'./gen{overallCounter}/CASTEP/castep.castep')[0] can work, AND return None even if the calculation is not finished.
                evaluated_config.get_potential_energy() # This seems to be a good complementary test.
                
                trainingTraj.write(evaluated_config)
            except:
                print(f"Castep calculation in folder {auxPath} not finished or still queueing, sleep 15 mins.")
                sleep(15*60)#sleep 15 mins
                pass
            
        trainingTraj.close()
        
        print(f"\n\n--------------------CYCLE {overallCounter}/{maxIterations} FINISHED--------------------\n\n\n\n")

