import numpy as np
from ase import atoms
from ase.io import read
from ase import neighborlist
from scipy import sparse

"""
30.03.2021
Check for two separating clusters. 
https://wiki.fysik.dtu.dk/ase/ase/neighborlist.html
"""

def does_it_break(atoms):
    
    #input 1.25 because to me it seemed the default 1.05 was too strict
    cutoffs = [1.25]*8
    neighborList = neighborlist.NeighborList(cutoffs, self_interaction=False, bothways=True)
    neighborList.update(atoms)
    matrix = neighborList.get_connectivity_matrix()
    n_components, component_list = sparse.csgraph.connected_components(matrix)
    if(n_components>1):
        print(f"There are {n_components} molecules in the system. Hence structure breaks.")
        return True
    elif(n_components==1):
        return False
    else:
        print(f'No. components is smalller than 1? Wtf')
        return

