import os
from ase import Atoms, Atom


from ase.calculators.castep import Castep
from ase.io import read, write
from ase.visualize import view
import numpy as np
from ase.io import Trajectory


mpirun = "mpirun"
mpirun_args = "-n 8"
castep = "castep.mpi"
os.environ['CASTEP_COMMAND'] = '{0} {1} {2}'.format(mpirun, mpirun_args, castep)
calculator = Castep(directory="./",
            cut_off_energy=250,
            spin_polarized=False,
#             spin_fix = -1,
#             spin = 0,
            opt_strategy='speed',
            xc_functional='PBE',
            write_checkpoint = 'none',
            #elec_energy_tol='0.0000001',
            max_scf_cycles=250,
            #fix_occupancy=False,
            calculate_stress=False,
            finite_basis_corr='automatic',
            smearing_width='0.04',
            #bs_nextra_bands = '20', #according to json keywords file the default is 4
            #fine_grid_scale=4,
            mixing_scheme='pulay',
#            metals_method = 'edft',
            #mix_history_length=20,
            #num_dump_cycles=0,
            kpoints_mp_spacing='0.040'
            #perc_extra_bands=200
            )


atoms = read("S8structure.xyz")
atoms.set_cell([24,24,24]) 
atoms.center()
atoms.set_pbc(True)


atoms.calc = calculator

atoms.get_potential_energy(force_consistent=True)





