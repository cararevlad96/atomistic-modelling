! gap_fit energy_parameter_name=energy force_parameter_name=forces stress_parameter_name=DontReadStresses do_copy_at_file=F sparse_separate_file=T gp_file=GAP.xml at_file=train.xyz default_sigma={0.002 0.04 0 0} config_type_sigma={isolated_atom:0.000001:0.00001:0:0} gap={distance_2b cutoff=4.5 covariance_type=ard_se delta=0.92965197447 theta_uniform=1.0 sparse_method=uniform add_species=F n_sparse=30 :\
soap cutoff=6 \
          covariance_type=dot_product \
          zeta=2 \
          delta=0.422 \
          atom_sigma=0.7 \
          l_max=6 \
          n_max=12 \
          n_sparse=200 \
          sparse_method=cur_points}
          
! quip E=T F=T atoms_filename=train.xyz param_filename=GAP.xml | grep AT | sed 's/AT//' > quip_train.xyz
! quip E=T F=T atoms_filename=InitialValidate.xyz param_filename=GAP.xml | grep AT | sed 's/AT//' > quip_validate.xyz
# angle_3b cutoff=2.5 covariance_type=ard_se delta=0.11486666666 theta_fac=0.5 add_species=T n_sparse=50 sparse_method=uniform :\
#  :\angle_3b cutoff=2.5 covariance_type=ard_se delta=0.14066666666 theta_fac=0.5 add_species=T n_sparse=50 sparse_method=uniform :\
# 

          
#  :\ :\