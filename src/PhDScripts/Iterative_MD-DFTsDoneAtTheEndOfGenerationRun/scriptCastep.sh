#!/bin/bash
#$ -pe smp 8
#$ -l h_rt=24:00:00
#$ -S /bin/bash
#$ -N CASTEP
#$ -j yes
#$ -cwd
#$ -q any

ulimit -s unlimited
export OMP_NUM_THREADS=1

python3 runCastep.py
