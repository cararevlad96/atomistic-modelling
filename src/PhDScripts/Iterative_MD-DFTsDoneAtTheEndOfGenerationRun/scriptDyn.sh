#!/bin/bash
#$ -pe smp 1
#$ -l h_rt=24:00:00
#$ -S /bin/bash
#$ -N MD
#$ -j yes
#$ -cwd
#$ -q any

ulimit -s unlimited
export OMP_NUM_THREADS=1

python3 dynamics.py
