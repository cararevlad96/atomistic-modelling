from ase.io import Trajectory
import glob
from ase.io.castep import read_castep_castep

def extract_best_of_n(traj_in, traj_out_name, min_n, max_n, key = 'config', has_isolated_atom = True):
    """
    Given a trajectory file which contains structures with random spin committee members arranged in a flat way, 
    but where each calculation has an atoms.info['config'] field, specifying which structure it pertains to.
    
    The method will write out trajectory files corresponding to the best of n from min_n to max_n.
    
    NOTE! Method is expecting the first structure to be the isolated atom by default.
    All other structures must have the corresponding key.
    
    traj_out_name is suggested to be under the format '02062021S8DatabaseDissociationRandomSpins-Validate-' where
    it will be pre-appended to 'BestOfAtMost{n_committee_members}CommitteeMembers.traj'
    
    Note: make sure to only pass those atoms which have the corresponding key. 
    
    """    

    for n_committee_members in range(min_n,max_n+1):

        traj_out = Trajectory(f'{traj_out_name}BestOfAtMost{n_committee_members}CommitteeMembers.traj','w')
        starting_config = 0
        
        if has_isolated_atom:
            traj_out.write(traj_in[0])
            starting_config = 1

               
        committee_count=1
        aux = [] # to hold all committee members until we decide which is the lowest energy one
        aux.append(traj_in[starting_config])
        current_config = traj_in[starting_config].info[key]
        for atoms in traj_in[starting_config:]:

            if(atoms.info[key] != current_config):#after finishing up with one structure, get the best of the n sub-members
                current_config = atoms.info[key]

                lowest_energy = aux[0].get_potential_energy()
                index_lowest_energy = 0

                if(len(aux)>1):
                    for index in range(1,len(aux)):
                        if(aux[index].get_potential_energy()<lowest_energy):
                            lowest_energy = aux[index].get_potential_energy()
                            index_lowest_energy = index

                traj_out.write(aux[index_lowest_energy])

                aux = []
                committee_count=1
                aux.append(atoms)

            else:
                if(committee_count < n_committee_members):

                    committee_count+=1
                    aux.append(atoms)


        #take care of the last distance as well
        lowest_energy = aux[0].get_potential_energy()
        index_lowest_energy = 0

        if(len(aux)>1):
            for index in range(1,len(aux)):
                if(aux[index].get_potential_energy()<lowest_energy):
                    lowest_energy = aux[index].get_potential_energy()
                    index_lowest_energy = index

        traj_out.write(aux[index_lowest_energy])
    

def create_traj(traj_in, traj_out, folder_name = ''):
    """
    Given a folder with 'config' subfolders, each corresponding to a structure, and each having further 'config'
    subfolders, corresponding to the random spin committee members,
    this method reads all calculations and appends to each atoms object the .info['config'] key
    corresponding to its parent folder.
 
    """   

    counter =0 
    for atoms in traj_in: 
        n_committee_members = glob.glob(f'./{folder_name}/config{counter}/config*')
        for n_member in range(len(n_committee_members)):
            try:
                with open(f'./{folder_name}/config{counter}/config{n_member}/castep.castep','r') as file:
                    try: 

                        atomsOut = read_castep_castep(file)[0]
                        atomsOut.info['distance']=atoms.info['distance']

                        atomsOut.info['config']=f'config{counter}'
#                         if 'initial_magmoms' in atomsOut.arrays: del atomsOut.arrays['initial_magmoms']
                        if 'initial_charges' in atomsOut.arrays: del atomsOut.arrays['initial_charges']

                        traj_out.write(atomsOut)
                    except:
                        print(f'config{counter}/config{n_member}' + " failed to converge")
                        pass 

            except:
                print('config{}'.format(counter) + " not present")
                pass
        counter +=1

    print(counter)

    traj_out.close()
