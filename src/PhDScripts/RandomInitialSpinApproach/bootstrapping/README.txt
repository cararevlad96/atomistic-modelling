11.06.2021

•	Bootstrapping for S6+S8: Find the mean & STD of the predictions' RMSEs of the potentials trained on a randomly picked committee member for each configuration, then the best of randomly picked pairs of committee members for each config, randomly picked triplets, and so on. Make a plot of mean + error bars of RMSE vs number of committee members. Have 100 models for each 