'''

11.06.2021

•	Bootstrapping for S6+S8: Find the mean & STD of the predictions' 
RMSEs of the potentials trained on a randomly picked committee member for
each configuration, then the best of randomly picked pairs of committee members 
for each config, randomly picked triplets, and so on. Make a plot of mean + error 
bars of RMSE vs number of committee members. Have 100 models for each 




input: 

an ASE trajectory file which contains all the calculations: each structure(configuration) has 15 committee members. 
The ordering within the trajectory file is a flattened out list of all the committee members of each config.
Each Atoms object has an .info property which tells which config (structure) it pertains to. 

S8 and S6 should be dealt with separately, because each have info['config'] = 'config0'... etc.




'''
from ase.io import Trajectory, write
from quippy.potential import Potential
import random
import numpy as np
import os
import subprocess
import time, glob, shutil
import sys

with open(f'outputBestOf{sys.argv[1]}.txt','w') as output:
    


    ''' 
    Method to write to an ouput file everything that's we want to print
    '''
    def decorator_to_file(func):
        def wrapper(*args):
            func(*args)
            output.write(str(*args))
            output.write('\n')
            output.flush()
        return wrapper

    print = decorator_to_file(print)

    n_repetitions = 100 #100 was suggested by Gabor
    max_n_committee = int(sys.argv[1]) #
    min_n_committee = int(sys.argv[1])
    os.environ["OMP_NUM_THREADS"] = "1" # since I am running this notebook directly on a node, I want to avoid hyperthreadding

    traj_S6 = Trajectory('18062021S6DatabaseDissociation-Train-AllCalculations-UpTo15Configs.traj')
    isolated_atom = traj_S6[0]
    traj_S6 = traj_S6[1:]#ignore isolated atom


    print('Retrieving S6 dissociation DFT energies')
    rigid_diss_S6 = Trajectory('20052021S6DissociationRandomSpins-BestOfAllCommitteeMembers.traj') # to test on
    dft_energies_diss_S6 = [atoms.get_potential_energy()/len(atoms) for atoms in rigid_diss_S6]

    print('Retrieving S8 dissociation DFT energies')
    traj_S8 = Trajectory('13062021S8DatabaseDissociation-Train-AllCommitteeMembers-UpTo24Members-OnlyStructsWith15OrOver-Curated.traj')
    traj_S8 = traj_S8[1:] #ignore isolated atom
    rigid_diss_S8 = Trajectory('20052021S8DissociationRandomSpins-BestOfAllCommitteeMembers.traj') # to test on
    dft_energies_diss_S8 = [atoms.get_potential_energy()/len(atoms) for atoms in rigid_diss_S8]




    '''method selecting the lowest energy structure out of given n structures'''
    def get_lowestE_struct(atoms_list):

        ordered_list = sorted(atoms_list, key = lambda atoms: atoms.get_potential_energy())

        return ordered_list[0]



    def get_rmse(x_ref, x_pred):
        """ Takes two datasets of the same shape and the RMSE"""

        x_ref = np.array(x_ref)
        x_pred = np.array(x_pred)

        if np.shape(x_pred) != np.shape(x_ref):
            raise ValueError('WARNING: not matching shapes in rms')

        error_2 = (x_ref - x_pred) ** 2

        rmse = np.sqrt(np.average(error_2))

        return rmse



    print('Creating the dictionary of configurations : list of committee members')
    config_dict = {}

    #get list of configs(structures)
    config_list_S6 = set([atoms.info['config'] for atoms in traj_S6])
    for config in config_list_S6:
        list_committee_members = [atoms for atoms in traj_S6 if atoms.info['config']==config]
        config_dict[f'S6-{config}'] = list_committee_members

    config_list_S8 = set([atoms.info['config'] for atoms in traj_S8])
    for config in config_list_S8:
        list_committee_members = [atoms for atoms in traj_S8 if atoms.info['config']==config]
        config_dict[f'S8-{config}'] = list_committee_members


    '''
    Getting data for two plots. The x coordinate is the same: the number of committee members taken within the best-of. 

    The y coordinate will be once with respect to the S6 dissociation, once with respect to the S8 dissociation.
    '''    
    overall_rmse_list_S6_rigid_diss = []
    yerr_rmse_list_S6_rigid_diss = []
    overall_rmse_list_S8_rigid_diss = []
    yerr_rmse_list_S8_rigid_diss = []

    for n_committee_members_to_sample in range(min_n_committee,max_n_committee+1):

        rmse_list_S6_rigid_diss = []
        rmse_list_S8_rigid_diss = []

            
        working_dir = f'./bestOf{n_committee_members_to_sample}'
        os.makedirs(working_dir)
        
        for repetition in range(0,n_repetitions):

            print(f'Starting repetition {repetition+1} of {n_repetitions} for {n_committee_members_to_sample} committee members.')


            current_training = []

            #for structure (config) in trajectory, take the best of [n_committee_members] randomly selected committee_members
            #and append them to your current train dataset
            for config, list_committee_members in config_dict.items():

                if(n_committee_members_to_sample<=len(list_committee_members)):
                    sampled_committee_members = random.sample(list_committee_members, n_committee_members_to_sample)
                else:
                    sampled_committee_members = random.sample(list_committee_members, len(list_committee_members))
                current_training.append(get_lowestE_struct(sampled_committee_members))



            '''
            #now that you have your current training trajectory, consisting of best of [n_committee_members_to_sample] calculations
            #per structure, create a GAP potential, and test it against the DFT energy for the S8 and S6 rigid dissociations
            #save the RMSE then in the outer loop find their mean and std.
            '''

            #write current training to current directory
            write(f'{working_dir}/train.xyz',isolated_atom)
            write(f'{working_dir}/train.xyz',current_training,append=True)


            #use subprocess to start the GAP fit
            
            shutil.copy('runGap.sh',working_dir)
            subprocess.run('bash runGap.sh', shell=True,cwd=working_dir)
            check = glob.glob(f'{working_dir}/GAP.xml')
            while(len(check)==0):
                print('GAP fit not yet finished (or there is no GAP job - do check), sleep 1 min.')
                time.sleep(5)
                check = glob.glob(f'{working_dir}/GAP.xml')





            ''' 
            Get the DFT-GAP energies on the S6 and S8 rigid dissociation
            '''
            print('Doing GAP on S6 dissociation')
            gap_energies_diss_S6 = []
            for atoms in rigid_diss_S6:

                atoms.calc = Potential(param_filename=f'{working_dir}/GAP.xml')
                gap_energies_diss_S6.append(atoms.get_potential_energy()/len(atoms))
            rmse_list_S6_rigid_diss.append(get_rmse(dft_energies_diss_S6,gap_energies_diss_S6))   


            gap_energies_diss_S8 = []
            print('Doing GAP on S8 dissociation')
            for atoms in rigid_diss_S8:

                atoms.calc = Potential(param_filename=f'{working_dir}/GAP.xml')
                gap_energies_diss_S8.append(atoms.get_potential_energy()/len(atoms))
            rmse_list_S8_rigid_diss.append(get_rmse(dft_energies_diss_S8,gap_energies_diss_S8))    




            #move GAP results to a corresponding folder

            dirs_to_move_to = f'./bestOf{n_committee_members_to_sample}/rep{repetition+1}'
            os.makedirs(dirs_to_move_to)
            files_to_move = glob.glob(f'{working_dir}/GAP*')
            files_to_move.extend(glob.glob(f'{working_dir}/train*'))
            for file in files_to_move:
                shutil.move(file, dirs_to_move_to)




        print(rmse_list_S6_rigid_diss)
        print(rmse_list_S8_rigid_diss)

        overall_rmse_list_S6_rigid_diss.append(np.mean(rmse_list_S6_rigid_diss))
        yerr_rmse_list_S6_rigid_diss.append(np.std(rmse_list_S6_rigid_diss))
        overall_rmse_list_S8_rigid_diss.append(np.mean(rmse_list_S8_rigid_diss))
        yerr_rmse_list_S8_rigid_diss.append(np.std(rmse_list_S8_rigid_diss))  

    print(overall_rmse_list_S6_rigid_diss)
    print(yerr_rmse_list_S6_rigid_diss)
    print(overall_rmse_list_S8_rigid_diss)
    print(yerr_rmse_list_S8_rigid_diss)


    
