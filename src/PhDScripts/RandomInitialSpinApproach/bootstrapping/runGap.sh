! gap_fit energy_parameter_name=energy force_parameter_name=forces stress_parameter_name=DontReadStresses do_copy_at_file=F sparse_separate_file=T gp_file=GAP.xml at_file=train.xyz default_sigma={0.005 0.1 0 0} config_type_sigma={isolated_atom:0.000001:0.00001:0:0} gap={distance_2b cutoff=4.5 covariance_type=ard_se delta=0.28618977319372146 theta_uniform=1.0 sparse_method=uniform add_species=F n_sparse=30  :\
soap cutoff=4.5 \
          covariance_type=dot_product \
          zeta=2 \
          delta=0.2629 \
          atom_sigma=0.7 \
          l_max=6 \
          n_max=12 \
          n_sparse=200 \
          sparse_method=cur_points}
          
# ! quip E=T F=T atoms_filename=train.xyz param_filename=GAP.xml | grep AT | sed 's/AT//' > quip_train.xyz
# ! quip E=T F=T atoms_filename=validate.xyz param_filename=GAP.xml | grep AT | sed 's/AT//' > quip_validate.xyz
