"""
Generates rattled dissociation configurations for Sulphur rings.

As dimers move away from the centre, they are rotate and streched & squeezed around the reference bond length.

Note script requires knowledge of which atoms are bonded. 
"""

import os
from ase import Atoms, Atom
from ase.calculators.castep import Castep
from ase.io import read, write
import numpy as np
from ase.io import Trajectory
from random import random
from ase.io.castep import write_castep_cell


atoms = read("./S6Ring.xyz")
atoms.set_cell([24,24,24]) 
atoms.center()
atoms.set_pbc(True)



def resetAtoms():
    atoms = read("./S6Ring.xyz")
    atoms.set_cell([24,24,24]) 
    atoms.center()
    atoms.set_pbc(True)
    return atoms


#these atoms were seen to be bonded by looking at the castep file
bonds = [[0, 1], [4, 3],[2,5]]
ringOrder = [3,2,5,0,1,4]


dissociationTraj = Trajectory('24062021generatedS6DatabaseRattled0,0P4,0P7to2inStepsOf0P1.traj','w') 
    

#gives a random number between 0 and 24
#this is used to randomly place the ghost H atom which defines how dimers rotate
#random() is a uniform distribution
def randGen(lower=0,upper=24): 
    n = lower + random()*(upper-lower)
    return n

#this is used for initialising spins # IRRLEVANT HERE
def randGenExp(lower=0,upper=0.9,scale=1): 
    n = lower + np.random.exponential(scale=scale)*(upper-lower)
    return n


    

nSubSteps = 4 
    
#what is the angle increment change per substep? 
#for small distances, it turns out 2 (for 08.01.2021) is too much. The energies would change too heavily
# for 12.01.2021 I used 0.5 although...that may be too small
anglOffset = 1 



#function to return a distance between 2.07421 (the bond length for S8 ring) and 1.90 (bond length for isolated dimer)
# when d=0 intrapolatedBL = 2.07421
# when d=largestDist intrapolatedBL = 1.90
# interpolation is a straight line ax+b so b=2.07421
# distance refers to the reaction coordinate

largestDist = 1.2 #From tests on 30.03.2021 we decided this was the best
def intrapolatedBondLength(distance):
    b = 2.07421 # bond length in S6
    a = (1.90 - b)/largestDist #1.90 is isolated dimer bond length
    if(distance<=largestDist):
        return a*distance+b
    else:
        return a*largestDist+b

#amount to specify the window by which we wish to change the intrapolatedbondLength of the S2 dimers
#the actual value of the bond length will be taken from an uniform distribution
# intrapolatedBondLength(distance) +- deviation
deviation = 0.05 



distances = [0,0.4,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2] 
counter = 0


for distance in distances:

    firstGo = False # at each distance DO first calculation where the bond are not rotated or stretched
    atoms = resetAtoms()



    atoms.append(Atom('H',[12,12,12])) #create fictional atom in the centre

    for bond in bonds:
        firstAtom = bond[0]
        secondAtom = bond[1]
        if(ringOrder.index(bond[1])!=(ringOrder.index(bond[0])+1)%len(ringOrder)):
            print("Indexing Alert")

        #i'm really not sure how this preserves the dimer bond lengths even with a constraint..but ok
        atoms.set_distance(atoms[-1].index,firstAtom,distance,fix=0,add=True,indices=bond,mic=True)
        #I'm not sure how this manages to move the second atom as well...

    del atoms[-1]


    undeformedAtoms = Atoms(atoms) # create copy of the undeformed config

    for step in range(0,nSubSteps):

        jobname = 'config{}'.format(counter)


        runFlag = True # check if after rotation atoms from different dimers are too close 


        atoms = undeformedAtoms

        if not firstGo:



            position = [randGen() for x in range(3)] # to randomise the rotation axis
            atoms.append(Atom('H',position))

            for bond in bonds:
                firstAtom = bond[0]
                secondAtom = bond[1]
                if(ringOrder.index(bond[1])!=(ringOrder.index(bond[0])+1)%len(ringOrder)):
                    print("Indexing Alert")  

                    
                angle = atoms.get_angle(atoms[-1].index,secondAtom,firstAtom) + anglOffset*step
                atoms.set_angle(atoms[-1].index,secondAtom,firstAtom,angle,add=False) # "If mask and indices are not set, only a3 is moved."
                                                                                      #  Although that's not what I've seen.


                #randomise bond length around reference values going from 2.06A for undeformed S8 ring
                #to 1.90 for isolated S2 dimer on a straight line
                referenceLength = intrapolatedBondLength(distance)
                lower = referenceLength - deviation
                upper = referenceLength + deviation
                atoms.set_distance(firstAtom,secondAtom,randGen(lower=lower,upper=upper),fix=0.5,add=False,mic=True)
                print(atoms.get_distance(firstAtom,secondAtom))

            del atoms[-1]


            # check two atoms from different dimers didn't get too close
            for atom in atoms:
                index = ringOrder.index(atom.index) #get the position of the atom in the order of the ring
                atomLeft = ringOrder[index-1]
                atomRight = ringOrder[(index+1)%len(ringOrder)]

                for otherAtom in atoms:
                    if((otherAtom.index != atom.index) and (otherAtom.index != atomLeft) and (otherAtom.index != atomRight)):
                        if(atoms.get_distance(atom.index,otherAtom.index)<3):
                                runFlag=False
                                print(atom.index,otherAtom.index)




        if runFlag:
            print('will run')

            atoms.info['distance'] = distance

            #calc = none so a castep calculation is not started when the atoms are written to trajectory
            atoms.calc = None
            dissociationTraj.write(atoms)



            # give me random values for atomic spins, between 0 and 1
            numberOfSpinConfigs = 15
            start_config = 0

            
            for spin_member_counter in range(start_config,start_config+numberOfSpinConfigs): 

                spins = [(np.random.uniform()*2-1) for i in range(0,len(atoms))]

                mpirun = "mpirun"
                mpirun_args = "-n 32"
                castep = "castep.mpi"
                os.environ['CASTEP_COMMAND'] = '{0} {1} {2}'.format(mpirun, mpirun_args, castep)
                calculator = Castep(directory=f'./config{counter}/config{spin_member_counter}',
                            cut_off_energy=250,
                            spin_polarized=True,
                            spin_fix = 1,
                            opt_strategy='speed',
                            xc_functional='PBE',
                            #elec_energy_tol='0.0000001',
                            max_scf_cycles=250,
                            fix_occupancy=False,
                            calculate_stress=False,
                            finite_basis_corr='automatic',
                            smearing_width='0.04',
                            #bs_nextra_bands = '20', #according to json keywords file the default is 4
                            #fine_grid_scale=4,
                            mixing_scheme='pulay',
            #                 metals_method = 'edft',
                            #mix_history_length=20,
                            #num_dump_cycles=0,
                            #mix_spin_amp=0.03,
                            kpoints_mp_spacing='0.040',
                            #perc_extra_bands=200
                            write_checkpoint='none'
                            )


                atoms.calc = calculator   
                atoms.calc.initialize()



                atoms.set_initial_magnetic_moments(spins)
                fd = open(calculator._directory + '/castep.cell','w')
                write_castep_cell(fd,atoms,castep_cell = atoms.calc.cell,magnetic_moments='initial')
                fd.close()

      
            
            counter += 1

            firstGo = False 



        else:
            print('will not run')

