"""
Scripts to analyse MD
"""


import matplotlib 
from ase.geometry.analysis import Analysis
from ase.neighborlist import build_neighbor_list 
import matplotlib.pyplot as plt
import os
import os.path
from matscipy.rings import ring_statistics
import numpy as np
from scipy import sparse

sulphur_cutoff = 1.2
max_ring_size = 1000





def run_analysis(trajs, labels, output_folder = 'AnglesBondsCoordinationRings'):

    """
    Create a folder where each file will have 4 corresponding analysis .txt files:
    rings, bonds, angles and coordination

    THIS MAY TAKE A LONG TIME - INTENDED TO SUBMIT TO COMPUTE NODE

    Pass trajectories
    """
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    if not isinstance(trajs, list):
        trajs = [trajs]
    if not isinstance(labels, list):
        labels = [labels]

    label_idx = 0
    for traj in trajs:

        label = labels[label_idx]
        print(label)

        all_angles = []
        all_bond_lengths = []
        all_coordination = []
        all_ring_counts = []

        for atoms in traj:

            angle_values, bond_lengths, coordination, ring_counts = analyse_snapshot(
                atoms)
            all_angles.extend(angle_values)
            all_bond_lengths.extend(bond_lengths)
            all_coordination.extend(coordination)
            all_ring_counts.extend(ring_counts)

        print(f'{len(all_ring_counts)} rings', f'{len(all_angles)} angles',
              f'{len(all_bond_lengths)} bonds', f'{len(all_coordination)} atoms')

        print(all_ring_counts, file=open(
            f"{output_folder}/output{label}Rings.txt", "w"))
        print(all_angles, file=open(
            f"{output_folder}/output{label}Angles.txt", "w"))
        print(all_bond_lengths, file=open(
            f"{output_folder}/output{label}Bonds.txt", "w"))
        print(all_coordination, file=open(
            f"{output_folder}/output{label}Coordination.txt", "w"))

        label_idx+=1





def analyse_snapshot(atoms):
    '''
    Returning angle values, bond lengths, coordination values and ring counts.
    ''' 
    larger_cutoffs = [sulphur_cutoff] * len(atoms)

    my_neighbor_list = build_neighbor_list(atoms, larger_cutoffs, skin=0, self_interaction=False,
                                           bothways=True)  # IF YOU DON"T SPECIFY SKIN=0 IT WILL INCLUDE
    # TERMS OUTSIDE THE BOND CUTOFF.

    coordination = [len(my_neighbor_list.get_neighbors(
        atom.index)[0]) for atom in atoms]

    structure = Analysis(atoms, nl=my_neighbor_list)

    try:
        bond_lengths = structure.get_values(
            structure.get_bonds('S', 'S'))[0]
    except:
        bond_lengths = None # No bonded atoms
    try:
        angle_values = structure.get_values(
            structure.get_angles('S', 'S', 'S'))[0]
    except:
        angle_values = None # No bonded trimers
        

    # array with count of number of sp rings of size given by position of number in array
    # array starts with 3 0s because there are no 0,1,2-atom rings
    rings_counts = ring_statistics(atoms, sulphur_cutoff * 2,
                                   maxlength=max_ring_size)
    rings_counts.resize(max_ring_size)

    #transform histogram to distribution
    aux = []
    ring_size = 0
    for count in rings_counts:
        aux.extend([ring_size] * int(count))
        ring_size += 1
    rings_counts = aux

    print('Returning angle values, bond lengths, coordination values and ring counts.')
    return (angle_values, bond_lengths, coordination, rings_counts)


def output_2_fold_coordination_files(trajs, labels, times, output_folder='AnglesBondsCoordinationRings'):
    '''
    Output a .txt file with count of 2-fold coordinated atoms.

    Can pass a list of sampling times, or a list of lists of sampling times, should you pass more than 1 trajectory.

    I'm doing this for a whole trajectory instead of a snapshot as I think building neighborlist may be more efficient this way. 
    '''

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    if not isinstance(trajs, list):
        trajs = [trajs]
    if not isinstance(labels, list):
        labels = [labels]

    if not isinstance(times, list):
        print('Times must be a list, or a list of lists.')
        exit()
    if not isinstance(times[0], list):  # means all trajs have the same times
        if len(trajs) != 1:
            times = [times]
            times *= int(len(trajs) / len(times))


    label_idx = 0
    for traj in trajs:

        two_fold_coordinations = []
        for atoms in traj:

            atoms.set_pbc(True)
            larger_cutoffs = [sulphur_cutoff] * len(atoms)
            my_neighbor_list = build_neighbor_list(atoms, larger_cutoffs, skin=0, self_interaction=False,
                                                   bothways=True)  # IF YOU DON"T SPECIFY SKIN=0 IT WILL INCLUDE
            # TERMS OUTSIDE THE BOND CUTOFF.
            coordination = [len(my_neighbor_list.get_neighbors(
                atom.index)[0]) for atom in atoms]
            n_two_fold = np.count_nonzero(np.array(coordination) == 2)
            two_fold_coordinations.append(n_two_fold)

        print(two_fold_coordinations, file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-twoFoldCoordAndTime.txt", "w"))
        print(times[label_idx], file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-twoFoldCoordAndTime.txt", "a"))

        label_idx += 1


def output_ring_counts_files(trajs, labels, times, output_folder='AnglesBondsCoordinationRings'):

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    if not isinstance(trajs, list):
        trajs = [trajs]
    if not isinstance(labels, list):
        labels = [labels]

    if not isinstance(times, list):
        print('Times must be a list, or a list of lists.')
        exit()
    if not isinstance(times[0], list):  # means all trajs have the same times
        if len(trajs) != 1:
            times = [times]
            times *= int(len(trajs) / len(times))


    label_idx = 0
    for traj in trajs:

        rings_counts = []

        for atoms in traj:

            # array with count of number of sp rings of size given by position of number in array
            # array starts with 3 0s because there are no 0,1,2-atom rings
            rings_count = ring_statistics(atoms, sulphur_cutoff * 2,
                                        maxlength=max_ring_size)
            rings_count.resize(max_ring_size)

            #transform histogram to distribution
            aux = []
            ring_size = 0
            for count in rings_count:
                aux.extend([ring_size] * int(count))
                ring_size += 1
            rings_counts.append(aux)



        print(rings_counts, file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-ringsCountsAndTime.txt", "w"))
        print(times[label_idx], file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-ringsCountsAndTime.txt", "a"))

        label_idx += 1



def plot_violin_plots(labels, output_folder='AnglesBondsCoordinationRings'):
    '''
    Make violin plots of angles, bonds, coordination and ring counts across trajectories
    from files in 
    '''


    angles_file_name = 'output{}Angles.txt'
    bonds_file_name = 'output{}Bonds.txt'
    coordination_file_name = 'output{}Coordination.txt'
    ring_counts_file_name = 'output{}Rings.txt'


    #angle and BL from Greenwood
    experimental_results = {'S2': [None, 188.9e-2], 'S6': [102.2, 205.7e-2], 'S7': [
        104.5, None], 'S8': [107.8, 203.7e-2], 'S10': [106.2, 205.6e-2], 'S-chain': [106.0, 206.6e-2]}
    min_angle = min(
        [values[0] for values in experimental_results.values() if values[0] is not None])
    max_angle = max(
        [values[0] for values in experimental_results.values() if values[0] is not None])
    min_bond = min(
        [values[1] for values in experimental_results.values() if values[1] is not None])
    max_bond = max(
        [values[1] for values in experimental_results.values() if values[1] is not None])
    angle_norm = matplotlib.colors.Normalize(vmin=min_angle, vmax=max_angle)
    bond_norm = matplotlib.colors.Normalize(vmin=min_bond, vmax=max_bond)
    cmap = matplotlib.cm.get_cmap('gist_rainbow')


    angles_across_trajectories = []
    bonds_across_trajectories = []
    coordination_across_trajectories = []
    rings_across_trajectories = []


    for label in labels:

        print(label)

        with open(output_folder + '/' + angles_file_name.format(label)) as _:
            all_angles = [float(number) for number in _.readline().strip(
                '[').strip('\n').strip(']').split(',')]
        with open(output_folder + '/' + bonds_file_name.format(label)) as _:
            all_bond_lengths = [float(number) for number in _.readline().strip(
                '[').strip('\n').strip(']').split(',')]
        with open(output_folder + '/' + coordination_file_name.format(label)) as _:
            all_coordination = [float(number) for number in _.readline().strip(
                '[').strip('\n').strip(']').split(',')]
        with open(output_folder + '/' + ring_counts_file_name.format(label)) as _:
            all_ring_counts = [float(number) for number in _.readline().strip(
                '[').strip('\n').strip(']').split(',')]

        angles_across_trajectories.append(all_angles)
        bonds_across_trajectories.append(all_bond_lengths)
        coordination_across_trajectories.append(all_coordination)
        rings_across_trajectories.append(all_ring_counts)

        print(f'{len(all_ring_counts)} rings', f'{len(all_angles)} angles',
              f'{len(all_bond_lengths)} bonds', f'{len(all_coordination)} atoms')


    fig, ax_list = plt.subplots(nrows=4, ncols=1, gridspec_kw={
                                'hspace': 0}, sharex='col')
    fig.set_size_inches(16, 20)


    ax_list[0].violinplot(angles_across_trajectories,
                        positions=range(len(labels)),
                        showmedians=True, showextrema=True)
    ax_list[1].violinplot(bonds_across_trajectories,
                          positions=range(len(labels)),
                        showmedians=True, showextrema=True)
    ax_list[2].violinplot(coordination_across_trajectories,
                          positions=range(len(labels)),
                        showmedians=True, showextrema=True)
    ax_list[3].violinplot(rings_across_trajectories,
                          positions=range(len(labels)),
                        showmedians=True, showextrema=True)

    colors = ['']
    for key, value in experimental_results.items():

        angle = value[0]
        bond = value[1]

        if angle is not None:
            ax_list[0].axhline(angle, ls='--', label=key,
                            color=cmap(angle_norm(angle)))

        if bond is not None:
            ax_list[1].axhline(bond, ls='--', label=key,
                            color=cmap(bond_norm(bond)))
    #         trans = transforms.blended_transform_factory(ax_list[0].get_yticklabels()[0].get_transform(), ax_list[0].transData)
    #         ax_list[0].text(1, angle, key, color="red", transform=trans,
    #             ha="right", va="center")

    #     ax_list[1].axhline()

    ax_list[0].set_xticks(list(range(0, 24, 2)))
    ax_list[0].xaxis.tick_top()
    ax_list[0].set_ylabel('Angle (deg)', fontsize=15, labelpad=20)
    ax_list[0].set_xlabel('GAP-RSS Iteration', fontsize=15, labelpad=20)
    ax_list[0].legend(fontsize=14, loc='upper right', bbox_to_anchor=(
        1.077, 1.05), framealpha=0.95, title='Experiment', title_fontsize=14, fancybox=True)
    ax_list[0].tick_params(axis='x', labeltop=True, labelsize=13)
    ax_list[0].tick_params(axis='y', labelsize=12)
    ax_list[0].xaxis.set_label_position('top')

    ax_list[1].set_ylabel('Bond Length (A)', fontsize=15, labelpad=20)
    ax_list[1].tick_params(axis='y', labelsize=12)
    ax_list[1].legend(fontsize=14, loc='upper right', bbox_to_anchor=(
        1.077, 1.05), framealpha=0.95, title='Experiment', title_fontsize=14, fancybox=True)

    ax_list[2].set_ylabel('Coordination Numbers', fontsize=15, labelpad=20)
    ax_list[2].tick_params(axis='y', labelsize=12)
    ax_list[2].tick_params(axis='x', labelsize=13)

    ax_list[3].set_ylabel('Rings size', fontsize=15, labelpad=20)
    ax_list[3].tick_params(axis='y', labelsize=12)
    ax_list[3].tick_params(axis='x', labelsize=13)


def output_2_fold_coordination_files_and_cluster_sizes(trajs, labels, times, output_folder='AnglesBondsCoordinationRings'):
    '''
    Output a .txt file with count of 2-fold coordinated atoms.

    Can pass a list of sampling times, or a list of lists of sampling times, should you pass more than 1 trajectory.

    I'm doing this for a whole trajectory instead of a snapshot as I think building neighborlist may be more efficient this way. 
    
    Also doing coord and cluster sized together because it saves time computing the neighbourlist 
    '''

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    if not isinstance(trajs, list):
        trajs = [trajs]
    if not isinstance(labels, list):
        labels = [labels]

    if not isinstance(times, list):
        print('Times must be a list, or a list of lists.')
        exit()
    if not isinstance(times[0], list):  # means all trajs have the same times
        if len(trajs) != 1:
            times = [times]
            times *= int(len(trajs) / len(times))

    label_idx = 0
    for traj in trajs:

        two_fold_coordinations = []
        cluster_sizes = []
        for atoms in traj:

            atoms.set_pbc(True)
            larger_cutoffs = [sulphur_cutoff] * len(atoms)
            my_neighbor_list = build_neighbor_list(atoms, larger_cutoffs, skin=0, self_interaction=False,
                                                   bothways=True)  # IF YOU DON"T SPECIFY SKIN=0 IT WILL INCLUDE
            # TERMS OUTSIDE THE BOND CUTOFF.
            coordination = [len(my_neighbor_list.get_neighbors(
                atom.index)[0]) for atom in atoms]
            n_two_fold = np.count_nonzero(np.array(coordination) == 2)
            two_fold_coordinations.append(n_two_fold)

            matrix = my_neighbor_list.get_connectivity_matrix()
            #or: matrix = neighborlist.get_connectivity_matrix(neighborList.nl)
            n_components, component_list = sparse.csgraph.connected_components(matrix)

            # component_list is a list of labels of length equal to number of atoms
            cluster_size = sorted([list(component_list).count(i) for i in set(component_list)])
            cluster_sizes.append(cluster_size)

        print(two_fold_coordinations, file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-twoFoldCoordAndTime.txt", "w"))
        print(times[label_idx], file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-twoFoldCoordAndTime.txt", "a"))

        print(cluster_sizes, file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-clusterSizes.txt", "w"))
        print(times[label_idx], file=open(
            f"{output_folder}/output{labels[label_idx]}-{times[label_idx][0]}ps-clusterSizes.txt", "a"))

        label_idx += 1




def get_data_from_lammps_script_output(files, search_line='Step Temp Press PotEng Density Time Elapsed CPU'):
    """
    Files is supposed to be a list of file names (usually slurm.**..*)
    This script is assumming the following command is used in lammps:
    'thermo_style    custom step temp press pe density time elapsed cpu' and potentially 'enthalpy' at the end
    """


    results_dict = {}
    for file in files:
        print(file)
        temps = []
        pressures = []
        densities = []
        times = []
        if 'Enthalpy' in search_line:
            enthalpy = []
        start = False
        with open(file) as fl:
            text = fl.readlines()
            for line in text:

                if start:
                    
                    
                    if 'slurmstepd' in line or 'srun' in line or 'ERROR' in line:
    #                     start= False
                        break
                    
                    if 'Loop' in line:
                        start = False
                    else:
                        quantities = line.split()
                        # print(quantities)
                        quantities = [float(quantity) for quantity in quantities]
                        temps.append(quantities[1])
                        pressures.append(quantities[2])
                        densities.append(quantities[4])
                        times.append([quantities[5]])
                        if 'Enthalpy' in search_line:
                            enthalpy.append([quantities[8]])

                        
                
                if search_line in line:
                    start = True


        #Check if a continution run is run in the same folder             
        key = file.split('/')[0]
        if key in results_dict.keys():
            results_dict[key]['temps'].extend(temps)
            results_dict[key]['pressures'].extend(pressures)
            results_dict[key]['densities'].extend(densities)
            if 'Enthalpy' in search_line:
                results_dict[key]['enthalpy'].extend(enthalpy)
            
            times = list(np.array(times)+results_dict[key]['times'][-1])
            results_dict[key]['times'].extend(times)
            print(f'Key {key} appearing again')
        else:
            if 'Enthalpy' in search_line:
                results_dict[file.split('/')[0]]={'temps':temps,
                                                'pressures':pressures,
                                                'densities':densities,
                                                'times':times,
                                                'enthalpy':enthalpy}
            else:
                results_dict[file.split('/')[0]]={'temps':temps,
                                                'pressures':pressures,
                                                'densities':densities,
                                                'times':times}
    
    return results_dict



def get_cluster_sizes_percetange_histogram(files,len_atoms=4096,bins=[1,2,5,6,8,9,20,40,100,500,1000,4096]):
    """
    Given a list of files containing the cluster sizes in the format of Ovito table export,
    create a histogram fo each cluster size bin.
    """

    from collections import Counter
    import collections
    # list_of_counters=[]
    list_of_histograms=[]
    for file in files:
        aux=[]
        with open(file) as fl:
#             print('Skipped over the first 2 lines:')
            line = fl.readline()
#             print(line)
            line = fl.readline()
#             print(line)
            while True:
                line = fl.readline()
                if not line:
                    break
                aux.append(int(line.split()[-1]))
        counts = collections.Counter(aux)
        aux=[]
        for cluster_size,count in counts.items():
            aux.extend([cluster_size]*count*cluster_size)
        histogram=np.histogram(aux,bins=bins)
        scaled_histogram=(histogram[0]*100/len_atoms,histogram[1])
    # #     list_of_counters.append(counter)
        list_of_histograms.append(scaled_histogram)
        
    return list_of_histograms
    """
    Example use for output:
    
    from scipy.ndimage.filters import uniform_filter1d


    smearing=1
    cmap = plt.get_cmap('viridis',len(list_of_histograms[0][0]))


    fig, ax = plt.subplots(1,1,figsize=(10,6))

    times = [i/100 for i in range(0,125,1)] # 0 to 1250 ps sampled every 10 ps
    counts_per_bin_per_time=[]
    for idx in range(len(list_of_histograms[0][0])):
        aux = []
        for histogram in list_of_histograms:
            aux.append(histogram[0][idx])
        counts_per_bin_per_time.append(aux)
        
        if (bins[idx+1]==bins[-1]):
            label = f'{bins[idx]}-{bins[idx+1]}'
        elif(bins[idx]!=bins[idx+1]-1):
            label = f'{bins[idx]}-{bins[idx+1]-1}'
        else:
            label = f'{bins[idx]}'
        
        aux = uniform_filter1d(aux,size=smearing)

        ax.plot(times,aux,label=label,lw=3,
                c=cmap(idx)
            )
           """
