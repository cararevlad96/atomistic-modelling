from PhDScripts import ovito_utils
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Obtain average structure factor')
    parser.add_argument('--input', type=str, help='Input file')
    parser.add_argument('--slice', type=str, help='eg. 3:10:2')
    parser.add_argument('--output', type=str, help='Name of output file')
    args = parser.parse_args()
    return args

def main():
    inputs = parse_args()
    
    file = inputs.input
    
    slice_input = inputs.slice.split(':')
    start_slice = int(slice_input[0])
    if slice_input[1] in ['None','none','']:
        stop_slice=None
    else:
        stop_slice = int(slice_input[1])
    step_slice = int(slice_input[2])
    frames_slice = slice(start_slice,stop_slice,step_slice)

    if not inputs.output:
        file_out_name = file.split('.')[0]+f"-sliced-by-{start_slice}P{stop_slice}P{step_slice}-structureFactor.pkl"
    
    else:
        file_out_name = inputs.output
        
    ovito_utils.obtain_structure_factor(file,frames_slice=frames_slice,file_out_name=file_out_name)
                                 
if __name__ == "__main__":
    main()