"""
methods to analyse gap-rss 
"""


import pandas as pd
import os
import time
from ase.io import read


def get_me_config_types_of_file(file):
    traj = read(file, ':')
    
    name = file.split('/')[-1].replace('.xyz', '')

    print(file, len(traj))
    config_types = {}

    config_types[name] = {}
    for atoms in traj:
        if 'config_type' in atoms.info:
            ct = atoms.info['config_type']
            if ct in config_types[name].keys():
                config_types[name][ct] += 1
            else:
                config_types[name][ct] = 1

        else:
            ct = 'None'
            if ct in config_types[name].keys():
                config_types[name][ct] += 1
            else:
                config_types[name][ct] = 1

    config_types[name]['ctime'] = time.ctime(os.path.getctime(file))

    return config_types


def get_me_config_types_of_files(files):
    df = pd.DataFrame(get_me_config_types_of_file(files[0])).T

    for file in files[1:]:

        #     if os.stat(file).st_size<5e7:
        print(file)
        aux = pd.DataFrame(get_me_config_types_of_file(file)).T
        df = df.append(aux)


    df = df.sort_values(by=['ctime'])
    df['ctime'] = df.pop('ctime')
    return df
