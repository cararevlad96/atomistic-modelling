"""
Given an .xyz in_file containing a molecular crystal, the code identifies the centre of each molecule,
scales the box along with the positions of the centres, keeping the molecules intact, then rattles the positions
of the centres, places the molecules, rattles and rotates them.

NOTE: This wrapping process does not work for all crystals. Some get unphysical. For example I did S6,S7...S20, but S13 failed to wrap properly. 
"""

from ase import neighborlist
from scipy import sparse
import ase.io
import ase 
from ase import Atoms
from ase.io import read,write
import random
from ase import neighborlist
from ase.build import molecule
from scipy import sparse
from ase.units import _amu
import numpy as np
import glob
from ase.geometry.analysis import Analysis
from ase.neighborlist import build_neighbor_list, neighbor_list
import matplotlib.pyplot as plt
import re
import os, os.path
from matscipy.rings import ring_statistics
from ase import Atom


sulphur_cutoff = 1.2
max_ring_size = 100

seeds = [2,502,58374,3058344,3882331,2674021692,394,6747,443,1295,230050450,1000034,32,67493]


def run(in_file, out_file, n_rattles, set_of_coefs=[0.01, 0.01, 0, 0.2], isotropic_cell_deviation = False):
    """
    set_of_coefs is a list of 4 floats:
        first float is the stdev with which you rattle the centres of the molecules
        second float is the stdev with which you rattled the atoms in a molecule
        third float is the length of the uniform distribution from which you sample the rotation angles of the molecule
        fourth float is the stdev with which you rattle the cell parameters 
    """
    traj = []
    
    if n_rattles>10:
        print('Only n_rattled<10 supported.')
        exit()

    def deviation(cell, sig = set_of_coefs[3]): # sig set empirically to have a bit of variation in densities
        """
        Cell is a 3x3 array

        isotropic_cell_deviation assumes orthorhombic cell
        """
        if isotropic_cell_deviation:
            # assumming orthorhombic cell
            # only modify [0][0] [1][1] and [2][2]; by the same amount
            a = cell[0]
            dev = random.gauss(cell[0][0], sig) - a[0]

            a += np.array([dev, 0, 0])
            b = cell[1] + np.array([0, dev, 0])
            c = cell[2] + np.array([0, 0, dev])
            return np.array([a, b, c])

        a = random.gauss(cell[0], sig)
        b = random.gauss(cell[1], sig)
        c = random.gauss(cell[2], sig)
        return np.array([a,b,c])


    #first append the non-rattled structure
    og_atoms = read(in_file)
    print('\n',len(og_atoms), in_file)




    og_atoms.info['config_type'] = ' crystal'
    ratio_g_cm3 = (og_atoms.get_masses().sum()*_amu*1000) / (og_atoms.get_volume()/(10**10)**3*(10**2)**3)
    print("Original density ",ratio_g_cm3)
    

#     traj.append(og_atoms)
# #         write(name_of_output,og_atoms,append=True)


    
    
    cutOff = [sulphur_cutoff]*len(og_atoms)
    

    
    # iter_seeds = iter(seeds)

    neighborList = neighborlist.NeighborList(cutOff, self_interaction=False, bothways=True,skin=0)
    neighborList.update(og_atoms)
    matrix = neighborList.get_connectivity_matrix()
    #or: matrix = neighborlist.get_connectivity_matrix(neighborList.nl)
    n_components, component_list = sparse.csgraph.connected_components(matrix)
    idx = 1
    molIdx = component_list[idx]
    print("There are {} molecules in the system".format(n_components))


    reference_n_components = n_components



    #16 molecules - 16 individual atoms objects
    molec_list = [Atoms(cell=og_atoms.get_cell(),pbc=True) for n_molec in range(n_components)]
    molec_idx = [f'molecule{i}' for i in range(n_components)]
    molec_dict = dict(zip(molec_idx,molec_list))

    for atom_idx in range(len(component_list)):
        # which molecule does the atom belong to
        molecule_index = component_list[atom_idx]
        key = f'molecule{molecule_index}'

        #append atom to the molecule object
        molec_dict[key].append(og_atoms[atom_idx])

    rings_list = []
    dummy_H_atoms_molecules_wrapped = Atoms(cell=og_atoms.get_cell(),pbc=True)
    for key, molecule in molec_dict.items():

        position_to_wrap = molecule.get_scaled_positions(wrap=True)[0]
        molecule_wrapped = Atoms(molecule)
        molecule_wrapped.wrap(pbc=True,center = position_to_wrap)
        rings_list.append(molecule_wrapped)
        
#         print(molecule_wrapped.get_positions())
        center_of_mass = molecule_wrapped.get_positions().mean(axis=0)
        dummy_H = Atom('H',position = center_of_mass)
#         print(dummy_H.position)
        dummy_H_atoms_molecules_wrapped.append(dummy_H)





    i = 0
    while(i<n_rattles):

        
        seed = seeds[i]
        
        centres = Atoms(dummy_H_atoms_molecules_wrapped)
        cell = og_atoms.get_cell()
#         cell = list(np.array(cell)+np.power((i+3)/10,4)*np.array(cell))
        cell = deviation(cell)



        centres.set_cell(cell,scale_atoms=True)
        centres.rattle(stdev=set_of_coefs[0],seed=seed)

#         print(cell)
        centres_pos = [list(atom.position) for atom in centres]






        atoms = Atoms(pbc=True,cell=cell)

        counter=0
        for ring in rings_list:

            rattled_ring = Atoms(ring)
            rattled_ring.rattle(stdev=set_of_coefs[1],seed=seed)
            rattled_ring.center(about=centres_pos[counter])
            #wiki says phi and psi go 0 to 360, theta 0 to 180
            #I don't want to rotate them that much, because I want a relatively high density, close to exp one
            phi = random.uniform(0,set_of_coefs[2])
            theta = random.uniform(0,set_of_coefs[2])
            psi = random.uniform(0,set_of_coefs[2])

            rattled_ring.euler_rotate(phi=phi,theta=theta,psi=psi,center='COP')
            atoms.extend(rattled_ring)


            counter+=1

        ratio_g_cm3 = (atoms.get_masses().sum() * _amu * 1000) / \
            (atoms.get_volume() / (10**10)**3 * (10**2)**3)

        happy = None
        while happy not in ['n', 'y']:
            happy = input(
                f"Are you happy with the random density {ratio_g_cm3}?(y/n)")
            if happy not in ['n', 'y']:
                print('Answer must be y or n.')
        if happy == 'n':
            continue # try random process again

        atoms.info['Density'] = ratio_g_cm3


        neighborList = neighborlist.NeighborList(
            cutOff, self_interaction=False, bothways=True, skin=0)
        neighborList.update(atoms)
        matrix = neighborList.get_connectivity_matrix(sparse=False)
        #or: matrix = neighborlist.get_connectivity_matrix(neighborList.nl)
        n_components, component_list = sparse.csgraph.connected_components(matrix)
        print("There are {} molecules in the system".format(n_components))




        rings_counts = np.zeros(max_ring_size)


        # array with count of number of sp rings of size given by position of number in array
        # array starts with 3 0s because there are no 0,1,2-atom rings
        aux = ring_statistics(atoms, sulphur_cutoff*2,maxlength = max_ring_size)
        aux.resize(max_ring_size) 
        rings_counts += aux




        #transform histogram to distribution
        aux =[]
        ring_size = 0
        for count in rings_counts:
            aux.extend([ring_size]*int(count))
            ring_size+=1
        print(len(aux), aux)
        n_rings = len(aux)

        
        #Spoke with Gabor - he said it is ok for rings to break. Some allotropes break easier and it is what it is
        if n_components != reference_n_components:
            print('Too many or few molecules, bonds were broken or formed. Retry the random process. YOU MAY NOT WANT THIS.')
            # continue
        elif n_rings != reference_n_components:
            print('Too many or few rings, bonds were broken or formed. Retry the random process. YOU MAY NOT WANT THIS.')
            # continue
        # else:
        i+=1

        print("Density ",ratio_g_cm3)

        atoms.info['config_type'] = 'Rattled crystal'
        
        atoms.wrap(pbc=True)
        traj.append(atoms)
    if len(traj)==1:
        write(out_file.replace('.xyz',f'{ratio_g_cm3:.4f}.xyz'), traj)
    else:
        write(out_file,traj)