import argparse
from ovito.io import import_file, export_file
from ovito.modifiers import ClusterAnalysisModifier
import numpy
import os,os.path


def parse_args():
    parser = argparse.ArgumentParser(description='Output cluster sizes files')
    parser.add_argument('--input', type=str, help='Input file')
    parser.add_argument('--slice', type=str, help='eg. 3:10:2')
    args = parser.parse_args()
    return args

def main():
    inputs = parse_args()
    
    file = inputs.input
    
    slice_input = inputs.slice.split(':')
    start_slice = int(slice_input[0])
    if slice_input[1] in ['None','none','']:
        stop_slice=None
    else:
        stop_slice = int(slice_input[1])
    step_slice = int(slice_input[2])
    frames_slice = slice(start_slice,stop_slice,step_slice)


    file_out_name = file.split('.')[0]+f"-sliced-by-{start_slice}P{stop_slice}P{step_slice}-structureFactor.pkl"

    
    if not os.path.exists('clusterSizes'):
        os.mkdir('clusterSizes')


    pipeline = import_file(file)
    pipeline.modifiers.append(ClusterAnalysisModifier(
        cutoff=2.6, 
        sort_by_size=True))
    
    
    for frame in list(range(pipeline.source.num_frames))[frames_slice]:
        data = pipeline.compute(frame)
        
        name = 'clusterSizes/'+file.split('.')[0]+f'-from{start_slice}to{stop_slice}in{step_slice}-clusterSizes.{frame}'
        
        # Export results of the clustering algorithm to a text file:
        export_file(data, name, 'txt/table', key='clusters')

    
if __name__ == "__main__":
    main()