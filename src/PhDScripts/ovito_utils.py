# REQUIRES THE OVITO CODE
# conda clean --index-cache
# conda install --strict-channel-priority -c https://conda.ovito.org -c conda-forge ovito==3.9.1 

"""
To do: 

Update for cluster sizes.
"""

import pickle
import numpy as np
from ovito.io import import_file, export_file
from ovito.modifiers import CoordinationAnalysisModifier
from ovito.modifiers import ClusterAnalysisModifier
from ovito.pipeline import Pipeline

def obtain_rdf(pipeline: Pipeline, frames_slice: slice) -> tuple:
    """
    Obtain the radial distribution function from a pipeline.
    :param pipeline: The pipeline to use.
    :param frame_slice: The slice of frames to use.
    :return: The radial distribution functions, the lists of bins and volumes.
    """

    # As as standard we use 15 Ang cutoff with 500 bins, we've seen 20 Ang doesn't change the result
    modifier = CoordinationAnalysisModifier(cutoff = 15.0, number_of_bins = 500)        

    # add the modifier to the pipeline
    pipeline.modifiers.append(modifier)
    
    volumes = []
    all_frames_rdf, all_frames_bins = [], []
    # run over all frames
    for frame in range(pipeline.source.num_frames)[frames_slice]:

        data = pipeline.compute(frame)

        volume = data.cell.volume

        print(data)
        print(data.tables)
        bins = data.tables['coordination-rdf'].xy()[:,0] * 0.1 # to get it to the right units since we want nanometers not ang
        rdf = data.tables['coordination-rdf'].xy()[:,1]

        all_frames_rdf.append(rdf)
        all_frames_bins.append(bins)
        volumes.append(volume)

    return all_frames_rdf, all_frames_bins, volumes
        

def obtain_structure_factor(file_in_name: str, frames_slice = slice(0,None,1), file_out_name = 'none', save_to_file = True):
    """
    Obtain the structure factor from a file.
    :param file_in_name: The name of the file to use.
    :param frames_slice: The slice of frames to use.
    :param file_out_name: The name of the file to save the structure factor to.
    :param save_to_file: Whether to save the structure factor to a file.
    :return: The structure factor.
    """

    pipeline = import_file(file_in_name)

    # print number of frames
    print("Total number of frames: %d" % pipeline.source.num_frames)
    # print the window of frame we are going to take and the step
    print(f"Sliced by: f{frames_slice}")
    # print the number of frames we are going to take
    print(f"Number of frames: {len(range(pipeline.source.num_frames)[frames_slice])}")

    # obtain the rdf
    all_frames_rdf, all_frames_bins, volumes = obtain_rdf(pipeline, frames_slice)

    n_atoms=pipeline.source.data.particles.count
    # compute the average volume
    avg_n_density = np.average(volumes)/n_atoms
        
    wavevectors=[wv/10 for wv in range(40,1200)] # in nanometers^-1
    avg_struct_factor = np.zeros(len(wavevectors))

    for rdf, bins in zip(all_frames_rdf, all_frames_bins):

        struct_fact = compute_structure_factor_from_rdf(bins,rdf,avg_n_density,wavevectors)

        avg_struct_factor+=np.array(struct_fact)
        

    avg_struct_factor/= len(range(pipeline.source.num_frames)[frames_slice])


    if save_to_file:
        # save the structure factor
        with open(file_out_name, "wb") as f:
            # dump the list to the file using pickle
            pickle.dump((avg_struct_factor, wavevectors, avg_n_density), f)

    return avg_struct_factor, wavevectors, avg_n_density




def compute_structure_factor_from_rdf(bins:list,rdf:list,n_density:float,wavevectors:list)->list:
    """
    Compute the structure factor from the radial distribution function.
    :param bins: The bins of the rdf.
    :param rdf: The rdf.
    :param n_density: The number density of the system.
    :param wavevectors: The wavevectors to use.
    :return: The structure factor.
    """

    avg_dr = np.average(np.array(bins[1:])-np.array(bins[:-1]))

    if len(bins)!=len(rdf):
        print('Distances and RDF lists should have the same length.')
        exit()
    
    
    struct_factor=[]
    
    for wv in wavevectors:
        strfctr=0
        for idx in range(len(bins)):
            strfctr+=4*np.pi*n_density*bins[idx]**2*(rdf[idx]-1)*np.sin(wv*bins[idx])/(bins[idx]*wv)*avg_dr
#             print((rdf[idx]-1)*np.sin(wv*distances[idx]))
        struct_factor.append(1+strfctr)
    
    return struct_factor

# needs documentation 
def compute_bond_lengths_and_angle_distribution(file,start_slice,stop_slice,step_slice,file_out_name, save_to_file = True):


    # Set up data pipeline:
    pipeline = import_file(file)

    pipeline.modifiers.append(CreateBondsModifier(cutoff = 2.6))

    # Calculate instantaneous bond angle distribution. 
    pipeline.modifiers.append(BondAnalysisModifier(length_cutoff=2.6,bins = 500))


    if stop_slice in ['None','none','']:
        stop_slice=pipeline.source.num_frames-1
    else:
        stop_slice = int(stop_slice)

    # Perform time averaging of the DataTable 'bond-angle-distr'.
    pipeline.modifiers.append(TimeAveragingModifier(interval=(start_slice,stop_slice),
                                                    sampling_frequency=step_slice,
                                                    operate_on=('table:bond-length-distr',
                                                               'table:bond-angle-distr')))


    # Convert to NumPy array and write data to a pickle file:
    bond_angles = pipeline.compute().tables['bond-angle-distr[average]'].xy()

    if save_to_file:
        # save the structure factor
        with open(file_out_name, "wb") as f:
            # dump the list to the file using pickle
            pickle.dump((bond_angles[:,0] , bond_angles[:,1] ), f)

    return bond_angles
    
def compute_average_rdf(file,start_slice,stop_slice,step_slice,file_out_name, save_to_file = True):
    # Load a simulation trajectory consisting of several frames:
    pipeline = import_file(file)
    
    # Insert the RDF calculation modifier into the pipeline:
    pipeline.modifiers.append(CoordinationAnalysisModifier(cutoff = 15, number_of_bins = 500))

    # Insert the time-averaging modifier into the pipeline, which accumulates
    # the instantaneous DataTable produced by the previous modifier and computes a mean histogram.
    pipeline.modifiers.append(TimeAveragingModifier(operate_on='table:coordination-rdf'))

    # Convert to NumPy array and write data to a pickle file:
    total_rdf = pipeline.compute().tables['coordination-rdf[average]'].xy()

    if save_to_file:
        # save the structure factor
        with open(file_out_name, "wb") as f:
            # dump the list to the file using pickle
            pickle.dump((total_rdf[:,0] , total_rdf[:,1] ), f)

    return total_rdf




