from ase.io import read, write
import os, os.path, subprocess
import sys
from ase import Atoms
import numpy as np
from PhDScripts.utils import get_density_in_gcm3
from PhDScripts import analyse_md

print(sys.argv)
if(len(sys.argv)!=2):
    print('Please provide file name as argument.')
    exit()
    
traj_name = sys.argv[1]

traj = read(traj_name,':')
do_you_want_coordination_and_cluster_analysis=True

config_types = sorted(set([atoms.info['config_type'] for atoms in traj]))
config_types_dict = dict(zip(list(config_types),list(range(len(config_types)))))
print(config_types_dict)

file_to_use = 'aux_file.xyz'
if os.path.exists(file_to_use):
    os.remove(file_to_use)
    
for atoms in traj:
    aux_atoms = Atoms(f'S{len(atoms)}',positions=atoms.get_positions(),cell=atoms.get_cell(),pbc=atoms.get_pbc())
    aux_atoms.info['config_type'] = atoms.info['config_type']
    aux_atoms.info['config_type_as_idx'] = config_types_dict[atoms.info['config_type']]
    aux_atoms.info['density']=round(get_density_in_gcm3(atoms),4)
    
    if do_you_want_coordination_and_cluster_analysis:
        angle_values, bond_lengths, coordination, rings_count = analyse_md.analyse_snapshot(atoms)
        
        avg_coordination = round(np.average(coordination),3)
        aux_atoms.info['avg_coord'] = avg_coordination

        n_eight_mem_rings = np.count_nonzero(np.array(rings_count)==8)
        aux_atoms.info['perc_n_eight_mem_rings'] = n_eight_mem_rings/(len(atoms)/8)
        
    write(file_to_use,aux_atoms,append=True)
    
    
subprocess.run('asap gen_desc --periodic --fxyz aux_file.xyz soap -c 5 -n 8 -l 4 -g 0.6',shell=True,check=True)


subprocess.run("asap map -f ASAP-desc.xyz -dm '[*]' -c config_type_as_idx -o chemiscope pca",shell=True,check=True)


