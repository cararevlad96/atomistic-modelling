from matplotlib.colors import LinearSegmentedColormap
try:
    import mpl_scatter_density # adds projection='scatter_density'
    density_plot = True
except ModuleNotFoundError:
    print('WARNING: Density plotting module not found, will use normal scatter plots.')
    density_plot = False
import matplotlib.pyplot as plt
import numpy as np
from ase.io import read
import pandas as pd
import json
from ase import Atoms
import os.path
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from matplotlib.lines import Line2D
import math

e_per_at_pbesol_minimised_alphaS8_on_Archer2 = -303.005479428
exp_enthalpy_between_alpha_and_beta_S8 = 0.0042 # From Meyer's Elemental Sulphur, 
                                            # converted from 0.096 kcal/gram-atom
castep_enthalpy_between_alpha_and_beta_S8 = 0.0012 #2  # from http://127.0.0.1:8889/edit/Calculations/25102021Potential/non-polarised/26012022-RE-minimising-the-experimental-crystals/ln03-_mnt_lustre_a2fs-work3_work_e89_e89_vc381_Calculations_25102021Potential_non-polarised_26012022-RE-minimising-the-experimental-crystals/run_castep_chunk_0_X7X1v6Dmrvmkkya3unvvu4xcbEcj_9s88GP09CwcglI%3D_xp_d_dbv/CASTEP_0qys9ltd/castep.castep

colors = ['blue','orange','red','magenta','brown','green']


def give_me_errors_dataframe(traj_in, dft_prefix = 'REF_', gap_prefix = 'calc_', scaled_sigmas = False, return_df = False):

    """
    scaled_sigmas refers to whether I scaled the energy,virial sigmas per config by the number of atoms, as I 
    saw that's the proper way to do it.
    I let the default to be False to be backward compatible.

    """
# traj_in = read('test2-dataset-with-sigmas-redo-REDO-noHighEnergyDimers/fitting.error_database.GAP_test-curated.xyz',':')

    if scaled_sigmas:
        es_name = 'avg energy sigma per atom'
        vs_name = 'avg virial sigma per atom'
    else:
        es_name = 'avg energy sigma'
        vs_name = 'avg virial sigma'
    
    sigmas = {}
    for atoms in traj_in:
        
        if f'{dft_prefix}energy' in atoms.info:

            if not scaled_sigmas:
                es = atoms.info['energy_sigma']
            else:
                es = atoms.info['energy_sigma']/len(atoms)
            eavg = atoms.info[f'{dft_prefix}energy']/len(atoms)
            ee = np.array([np.abs((atoms.info[f'{dft_prefix}energy']-atoms.info[f'{gap_prefix}energy'])/len(atoms))])
        else:
            es = np.nan
            eavg = np.nan
            ee = np.nan
            
        if f'{dft_prefix}stress' in atoms.info:

            if not scaled_sigmas:
                vs = atoms.info['virial_sigma']
            else:
                vs = atoms.info['virial_sigma']/len(atoms)
                
            vavg = np.mean(np.abs(atoms.info[f'{dft_prefix}stress']))*(-1*atoms.get_volume())/len(atoms)
            ve = np.array((atoms.info[f'{dft_prefix}stress']-atoms.info[f'{gap_prefix}stress'])/len(atoms)*(-1*atoms.get_volume()))
        else:
            vs=np.nan
            ve=np.nan
            vavg=np.nan
            
            
        if f'{dft_prefix}forces' in atoms.arrays:
            if 'force_sigma' in atoms.info:
                fs = atoms.info['force_sigma']*len(atoms)
                
                favg = np.sum(np.linalg.norm(atoms.arrays[f'{dft_prefix}forces'],axis=1))
                fe = np.linalg.norm(atoms.arrays[f'{dft_prefix}forces']-atoms.arrays[f'{gap_prefix}forces'],axis=1)

            elif 'force_atom_sigma' in atoms.arrays:
                fs = np.sum(atoms.arrays['force_atom_sigma'])
                
                favg = np.sum(np.linalg.norm(atoms.arrays[f'{dft_prefix}forces'],axis=1))
                fe = np.linalg.norm(atoms.arrays[f'{dft_prefix}forces']-atoms.arrays[f'{gap_prefix}forces'],axis=1)

        else:
            fs=np.nan
            fe=np.nan
            favg=np.nan
        
            
        if atoms.info['config_type'] not in sigmas.keys():
            sigmas[atoms.info['config_type']] = {'energy_error (eV/atom)':ee,es_name:es, 'avg_energy (eV/atom)': eavg,
                                                'force_error (eV/A)':fe,'avg force sigma':fs,'avg_force (eV/A)':favg,
                                                'virial_error (eV/atom)':ve,vs_name:vs,'avg_virial (eV/atom)':vavg,
                                                'nr_struct':1,'nr_atoms':len(atoms)}
        else:
            
            if ee is not np.nan:
                sigmas[atoms.info['config_type']]['energy_error (eV/atom)']= np.concatenate([ee,sigmas[atoms.info['config_type']]['energy_error (eV/atom)']])
            else:
                sigmas[atoms.info['config_type']]['energy_error (eV/atom)'] = np.nan
            sigmas[atoms.info['config_type']][es_name] += es
            sigmas[atoms.info['config_type']]['avg_energy (eV/atom)'] += eavg
            sigmas[atoms.info['config_type']]['force_error (eV/A)'] = np.concatenate([fe,sigmas[atoms.info['config_type']]['force_error (eV/A)']])
            sigmas[atoms.info['config_type']]['avg force sigma'] += fs
            sigmas[atoms.info['config_type']]['avg_force (eV/A)'] += favg
            if ve is not np.nan:
                sigmas[atoms.info['config_type']]['virial_error (eV/atom)'] = np.concatenate([ve,sigmas[atoms.info['config_type']]['virial_error (eV/atom)']])
            else:
                sigmas[atoms.info['config_type']]['virial_error (eV/atom)'] = np.nan
            sigmas[atoms.info['config_type']][vs_name] += vs
            sigmas[atoms.info['config_type']]['avg_virial (eV/atom)'] += vavg
            sigmas[atoms.info['config_type']]['nr_struct'] +=1
            sigmas[atoms.info['config_type']]['nr_atoms'] +=len(atoms)
            

    df = pd.DataFrame(sigmas).T

    df['energy_error (eV/atom)'] = df['energy_error (eV/atom)'].map(lambda array: np.sqrt(np.mean(array ** 2)))
    df[es_name] = df[es_name]/df['nr_struct']
    df['avg_energy (eV/atom)'] = df['avg_energy (eV/atom)']/df['nr_struct']
    df['force_error (eV/A)'] = df['force_error (eV/A)'].map(lambda array: np.sqrt(np.mean(array ** 2)))
    df['avg force sigma'] = df['avg force sigma']/df['nr_atoms']
    df['avg_force (eV/A)'] = df['avg_force (eV/A)']/df['nr_atoms']
    df['virial_error (eV/atom)'] = df['virial_error (eV/atom)'].map(lambda array: np.sqrt(np.mean(array ** 2)))
    df[vs_name] = df[vs_name] / df['nr_struct']
    df['avg_virial (eV/atom)'] = df['avg_virial (eV/atom)']/df['nr_struct']
    
    df['avg_energy (eV/atom)'] -= e_per_at_pbesol_minimised_alphaS8_on_Archer2
    df = df.rename(
        columns={'avg_energy (eV/atom)': 'avg_energy over min. alpha S8 (eV/atom)'})


    # df = df.rename(index={'DFT-MD-alphaS8-Heating-Set-1': 'DFT-MD-alphaS8-Heating \n T<500K',
    #             'DFT-MD-alphaS8-Heating-Set-2': 'DFT-MD-alphaS8-Heating \n T-in-[500K,1300K]',
    #             'DFT-MD-alphaS8-Heating-Set-3': 'DFT-MD-alphaS8-Heating \n T-in-[1300K,2500K]',
    #             'DFT-MD-alphaS8-Heating-Set-4': 'DFT-MD-alphaS8-Heating \n T>2500K',
    #             'buildcell':'Initial random buildcell',
    #             'Molecule dissociation':'Molecules dissociation',
    #             'single_atom':'Single atom',
    #             'dimer':'Dimers',
    #             'Experimental crystal':'Experimental crystals',
    #             'Rattled crystal':'Rattled crystals',
    #                     'S8-heatup-iterative-MD':'alpha S8 heatup from iterative MD'})

    if return_df:
        return df
    else:
        return stylise_error_df(df, scaled_sigmas)

def stylise_error_df(df, scaled_sigmas): 
    if scaled_sigmas:
        es_name = 'avg energy sigma per atom'
        vs_name = 'avg virial sigma per atom'
    else:
        es_name = 'avg energy sigma'
        vs_name = 'avg virial sigma'
    df = df.style.set_properties(
        **{'color': 'black',
        'border': '1.5px black solid '}
    ).set_table_styles({
        "energy_error (eV/atom)" : [{'selector': 'td',
            'props': 'border-left: 3px solid black'}],
        "force_error (eV/A)" : [{'selector': 'td',
            'props': 'border-left: 3px solid black '}],
        "virial_error (eV/atom)" : [{'selector': 'td',
            'props': 'border-left: 3px solid black '}],
        "nr_struct" : [{'selector': 'td',
            'props': 'border-left: 3px solid black '}],
        '' : [{
        'selector': 'th',
        'props': 'border : 1px black solid !important'}]
        }).format({
        'energy_error (eV/atom)': '{:,.5f}'.format,
        'force_error (eV/A)': '{:,.5f}'.format,
        'virial_error (eV/atom)': '{:,.5f}'.format,
        es_name: '{:,.5f}'.format,
        'avg force sigma': '{:,.5f}'.format,
        vs_name: '{:,.5f}'.format,
        'avg_energy over min. alpha S8 (eV/atom)': '{:,.5f}'.format,
        'avg_force (eV/A)': '{:,.5f}'.format,
        'avg_virial (eV/atom)': '{:,.5f}'.format,
        'fractional force error': '{:,.5f}'.format
    })

    return df


def give_me_errors_dataframe_config_type_wide_sigmas(traj_in, sigmas_dict, dft_prefix='REF_', gap_prefix='calc_', return_df=False):
    """
    Same as give_me_errors_dataframe table but this is for the case where
    you don't have sigmas per config, but may per whole config type

    Sigmas are passed as a dict where first element is energy sigma, then force sigma and virial sigma
    

    """


    es_name = 'avg energy sigma'
    vs_name = 'avg virial sigma'
    errors = {}

    for atoms in traj_in:
        ct = atoms.info['config_type']

        if f'{dft_prefix}energy' in atoms.info:
            es = sigmas_dict[ct][0]
            eavg = atoms.info[f'{dft_prefix}energy'] / len(atoms)
            ee = np.array([np.abs(
                (atoms.info[f'{dft_prefix}energy'] - atoms.info[f'{gap_prefix}energy']) / len(atoms))])
        else:
            es = np.nan
            eavg = np.nan
            ee = np.nan
        #isolated atom should not have forces and virials
        if ct!='single_atom' and ct!='isolated_atom' and len(atoms)!=1:
            
            fs = sigmas_dict[ct][1]
            favg = np.sum(np.linalg.norm(
                atoms.arrays[f'{dft_prefix}forces'], axis=1))
            fe = np.linalg.norm(
                atoms.arrays[f'{dft_prefix}forces'] - atoms.arrays[f'{gap_prefix}forces'], axis=1)
            
            #dimers and isolated molecules should not have virials
            if ct!='dimer' and ct!='Dimers' and len(atoms)!=2 and 'dissociation' not in ct and f'{dft_prefix}stress' in atoms.info:

                vs = sigmas_dict[ct][2]
                vavg = np.mean(np.abs(
                    atoms.info[f'{dft_prefix}stress'])) * (-1 * atoms.get_volume()) / len(atoms)
                ve = np.array((atoms.info[f'{dft_prefix}stress'] - atoms.info[f'{gap_prefix}stress']) / len(
                    atoms) * (-1 * atoms.get_volume()))
            else:
                vs = np.nan
                ve = np.nan
                vavg = np.nan
        else:
            fs = np.nan
            fe = np.nan
            favg = np.nan
            vs = np.nan
            ve = np.nan
            vavg = np.nan

        
        if atoms.info['config_type'] not in errors.keys():
            errors[atoms.info['config_type']] = {'energy_error (eV/atom)': ee, es_name: es, 'avg_energy (eV/atom)': eavg,
                                                 'force_error (eV/A)': fe, 'avg force sigma': fs, 'avg_force (eV/A)': favg,
                                                 'virial_error (eV/atom)': ve, vs_name: vs, 'avg_virial (eV/atom)': vavg,
                                                 'nr_struct': 1, 'nr_atoms': len(atoms)}
        else:

            errors[atoms.info['config_type']]['energy_error (eV/atom)'] = np.concatenate(
                [ee, errors[atoms.info['config_type']]['energy_error (eV/atom)']])
            errors[atoms.info['config_type']][es_name] += es
            errors[atoms.info['config_type']]['avg_energy (eV/atom)'] += eavg
            errors[atoms.info['config_type']]['force_error (eV/A)'] = np.concatenate(
                [fe, errors[atoms.info['config_type']]['force_error (eV/A)']])
            errors[atoms.info['config_type']]['avg force sigma'] += fs
            errors[atoms.info['config_type']]['avg_force (eV/A)'] += favg
            if ve is not np.nan:
                errors[atoms.info['config_type']]['virial_error (eV/atom)'] = np.concatenate(
                    [ve, errors[atoms.info['config_type']]['virial_error (eV/atom)']])
            else:
                errors[atoms.info['config_type']
                       ]['virial_error (eV/atom)'] = np.nan
            errors[atoms.info['config_type']][vs_name] += vs
            errors[atoms.info['config_type']]['avg_virial (eV/atom)'] += vavg
            errors[atoms.info['config_type']]['nr_struct'] += 1
            errors[atoms.info['config_type']]['nr_atoms'] += len(atoms)

    df = pd.DataFrame(errors).T

    df['energy_error (eV/atom)'] = df['energy_error (eV/atom)'].map(
        lambda array: np.sqrt(np.mean(array ** 2)))
    df[es_name] = df[es_name] / df['nr_struct']
    df['avg_energy (eV/atom)'] = df['avg_energy (eV/atom)'] / df['nr_struct']
    df['force_error (eV/A)'] = df['force_error (eV/A)'].map(
        lambda array: np.sqrt(np.mean(array ** 2)))
    df['avg force sigma'] = df['avg force sigma'] / df['nr_struct']
    df['avg_force (eV/A)'] = df['avg_force (eV/A)'] / df['nr_atoms']
    df['virial_error (eV/atom)'] = df['virial_error (eV/atom)'].map(
        lambda array: np.sqrt(np.mean(array ** 2)))
    df[vs_name] = df[vs_name] / df['nr_struct']
    df['avg_virial (eV/atom)'] = df['avg_virial (eV/atom)'] / df['nr_struct']

    df['avg_energy (eV/atom)'] -= e_per_at_pbesol_minimised_alphaS8_on_Archer2
    df = df.rename(
        columns={'avg_energy (eV/atom)': 'avg_energy over min. alpha S8 (eV/atom)'})

    # df = df.rename(index={'DFT-MD-alphaS8-Heating-Set-1': 'DFT-MD-alphaS8-Heating \n T<500K',
    #             'DFT-MD-alphaS8-Heating-Set-2': 'DFT-MD-alphaS8-Heating \n T-in-[500K,1300K]',
    #             'DFT-MD-alphaS8-Heating-Set-3': 'DFT-MD-alphaS8-Heating \n T-in-[1300K,2500K]',
    #             'DFT-MD-alphaS8-Heating-Set-4': 'DFT-MD-alphaS8-Heating \n T>2500K',
    #             'buildcell':'Initial random buildcell',
    #             'Molecule dissociation':'Molecules dissociation',
    #             'single_atom':'Single atom',
    #             'dimer':'Dimers',
    #             'Experimental crystal':'Experimental crystals',
    #             'Rattled crystal':'Rattled crystals',
    #                     'S8-heatup-iterative-MD':'alpha S8 heatup from iterative MD'})

    if return_df:
        return df
    else:
        return stylise_error_df(df,False)


def plot_force_component_parity_plots(traj_in, dft_prefix = 'REF_', model_prefix = 'calc_', sigma_on_plot = False, contains_config_types = True, density_plot = True):


    if sigma_on_plot:
        sigma_dict = get_sigma_dict(traj_in)

    force_components = {}

    for atoms in traj_in:
        if len(atoms)==1:
            continue
        if not contains_config_types:
            atoms.info['config_type'] = 'all_configs'
        if atoms.info['config_type'] in force_components.keys():
            config_type = atoms.info['config_type']
            if f'{dft_prefix}forces' in atoms.arrays:
                ref_force_comp = atoms.arrays[f'{dft_prefix}forces'].flatten()
                model_force_comp = atoms.arrays[f'{model_prefix}forces'].flatten()
                force_components[config_type]['DFT_force_comp'] = np.concatenate(
                    [force_components[config_type]['DFT_force_comp'], ref_force_comp])
                force_components[config_type]['model_force_comp'] = np.concatenate(
                    [force_components[config_type]['model_force_comp'], model_force_comp])

        else:
            config_type = atoms.info['config_type']
            if f'{dft_prefix}forces' in atoms.arrays:
                ref_force_comp = atoms.arrays[f'{dft_prefix}forces'].flatten()
                model_force_comp = atoms.arrays[f'{model_prefix}forces'].flatten()
                force_components[config_type] = {}
                force_components[config_type]['DFT_force_comp'] = np.array(
                    ref_force_comp)
                force_components[config_type]['model_force_comp'] = np.array(
                    model_force_comp)

    # "Viridis-like" colormap with white background
    white_viridis = LinearSegmentedColormap.from_list('white_viridis', [
        (0, '#ffffff'),
        (1e-20, '#440053'),
        (0.2, '#404388'),
        (0.4, '#2a788e'),
        (0.6, '#21a784'),
        (0.8, '#78d151'),
        (1, '#fde624'),
    ], N=256)


    nr_config_types = len(force_components)
    print(nr_config_types)

    if density_plot:
        fig, axs = plt.subplots(nrows=nr_config_types, ncols=1, figsize=(6, 7 * nr_config_types), gridspec_kw={'hspace': 0.4},
                                subplot_kw={'projection': 'scatter_density'}) # squeeze false so they return a list even when theres only one element
    else:
        fig, axs = plt.subplots(nrows=nr_config_types, ncols=1, figsize=(6, 7 * nr_config_types), gridspec_kw={'hspace': 0.4})
        pass

    try:
        axs[0]
    except:
        axs = [axs]
        
    axs_idx = 0

    for config_type, forces_dict in force_components.items():

        if density_plot:
            density = axs[axs_idx].scatter_density(forces_dict['DFT_force_comp'],
                                                forces_dict['model_force_comp'], cmap=white_viridis,
                                                dpi=30)
            fig.colorbar(density, label='Number of points per pixel', ax=axs[axs_idx])
        else:
            axs[axs_idx].scatter(forces_dict['DFT_force_comp'],forces_dict['model_force_comp'])
            pass

        axs[axs_idx].set_xlabel('DFT force component (ev/A)', fontsize=14)
        if model_prefix=='calc_':
            model_prefix='GAP_' # historical artefact
        axs[axs_idx].set_ylabel(f'{model_prefix.strip("_")} force component (ev/A)', fontsize=14)


        if sigma_on_plot:
            if (len(sigma_dict[config_type])>1):
                axs[axs_idx].text(0.1, 0.9, f'force_sigma = {sigma_dict[config_type][1]}',
                                transform=axs[axs_idx].transAxes, fontsize='large')

        if config_type == 'buildcell':
            axs[axs_idx].set_title('Initial random buildcell', fontsize=15)
        else:
            axs[axs_idx].set_title(config_type, fontsize=15)

        low_x, high_x = axs[axs_idx].get_xlim()
        low_y, high_y = axs[axs_idx].get_ylim()
        low = min(low_x, low_y)
        high = max(high_x, high_y)
        axs[axs_idx].set_xlim(low,high)
        axs[axs_idx].set_ylim(low,high)
        axs[axs_idx].plot([0, 1], [0, 1], ls='--', c='k',transform=axs[axs_idx].transAxes)

        # add text about RMSE
        _rms = rms_dict(forces_dict['DFT_force_comp'], forces_dict['model_force_comp'])
        rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV/Ang'
        axs[axs_idx].text(0.9, 0.1, rmse_text, transform=axs[axs_idx].transAxes, fontsize='large', horizontalalignment='right',
                verticalalignment='bottom')
        axs_idx += 1
    return fig, axs


def plot_energy_parity_plots(traj_in, dft_prefix='REF_', model_prefix='calc_', sigma_on_plot=False, contains_config_types=True, **kwargs):

    if sigma_on_plot:
        sigma_dict = get_sigma_dict(traj_in)

    energies_dict = {}

    for atoms in traj_in:
        if len(atoms) == 1:
            continue
        if not contains_config_types:
            atoms.info['config_type'] = 'all_configs'
        if atoms.info['config_type'] in energies_dict.keys():
            config_type = atoms.info['config_type']
            if f'{dft_prefix}energy' in atoms.info:
                ref_energy = atoms.info[f'{dft_prefix}energy']/len(atoms)
                model_energy = atoms.info[f'{model_prefix}energy'] / len(atoms)
                energies_dict[config_type]['ref_energy'].append(ref_energy)
                energies_dict[config_type]['model_energy'].append(model_energy)

        else:
            config_type = atoms.info['config_type']
            if f'{dft_prefix}energy' in atoms.info:
                ref_energy = atoms.info[f'{dft_prefix}energy']/len(atoms)
                model_energy = atoms.info[f'{model_prefix}energy'] / len(atoms)
                energies_dict[config_type] = {}
                energies_dict[config_type]['ref_energy'] = [ref_energy]
                energies_dict[config_type]['model_energy'] = [model_energy]

    # "Viridis-like" colormap with white background
    white_viridis = LinearSegmentedColormap.from_list('white_viridis', [
        (0, '#ffffff'),
        (1e-20, '#440053'),
        (0.2, '#404388'),
        (0.4, '#2a788e'),
        (0.6, '#21a784'),
        (0.8, '#78d151'),
        (1, '#fde624'),
    ], N=256)

    nr_config_types = len(energies_dict)
    print(nr_config_types)

    if 'density_plot' in kwargs:
        density_plot = kwargs['density_plot']

    if density_plot:
        fig, axs = plt.subplots(nrows=nr_config_types, ncols=1, figsize=(6, 7 * nr_config_types), gridspec_kw={'hspace': 0.4},
                                subplot_kw={'projection': 'scatter_density'})  # squeeze false so they return a list even when theres only one element
    else:
        fig, axs = plt.subplots(nrows=nr_config_types, ncols=1, figsize=(
            6, 7 * nr_config_types), gridspec_kw={'hspace': 0.4})
        pass

    try:
        axs[0]
    except:
        axs = [axs]

    axs_idx = 0

    for config_type, energy_dict in energies_dict.items():

        if density_plot:
            density = axs[axs_idx].scatter_density(energy_dict['ref_energy'],
                                                   energy_dict['model_energy'], cmap=white_viridis,
                                                   dpi=30)
            fig.colorbar(
                density, label='Number of points per pixel', ax=axs[axs_idx])
        else:
            axs[axs_idx].scatter(energy_dict['ref_energy'],
                                 energy_dict['model_energy'])
            pass

        axs[axs_idx].set_xlabel('DFT energy (eV/atom)', fontsize=14)
        if model_prefix=='calc_':
            model_prefix='GAP_' # historical artefact
        axs[axs_idx].set_ylabel(f'{model_prefix.strip("_")} energy (eV/atom)', fontsize=14)

        if sigma_on_plot:
            if (len(sigma_dict[config_type]) > 1):
                axs[axs_idx].text(0.1, 0.9, f'energy_sigma = {sigma_dict[config_type][0]}',
                                  transform=axs[axs_idx].transAxes, fontsize='large')

        if config_type == 'buildcell':
            axs[axs_idx].set_title('Initial random buildcell', fontsize=15)
        else:
            axs[axs_idx].set_title(config_type, fontsize=15)

        low_x, high_x = axs[axs_idx].get_xlim()
        low_y, high_y = axs[axs_idx].get_ylim()
        low = min(low_x, low_y)
        high = max(high_x, high_y)
        axs[axs_idx].set_xlim(low, high)
        axs[axs_idx].set_ylim(low, high)
        axs[axs_idx].plot([0, 1], [0, 1], ls='--', c='k',
                          transform=axs[axs_idx].transAxes)


        # add text about RMSE
        _rms = rms_dict(energy_dict['ref_energy'], energy_dict['model_energy'])
        rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV'
        axs[axs_idx].text(0.9, 0.1, rmse_text, transform=axs[axs_idx].transAxes, fontsize='large', horizontalalignment='right',
                verticalalignment='bottom')
        axs_idx += 1


    return fig, axs


def get_sigma_dict(traj_in):

    sigma_dict = {}

    for atoms in traj_in:
        if atoms.info['config_type'] not in sigma_dict.keys():
            if 'energy_sigma' in atoms.info:
                es = atoms.info['energy_sigma']
            else:
                es =  np.nan
            if 'force_sigma' in atoms.info:
                fs = atoms.info['force_sigma']

                if 'virial_sigma' in atoms.info:
                    ss = atoms.info['virial_sigma']
                    sigma_dict[atoms.info['config_type']] = [es, fs, ss]
                else:
                    sigma_dict[atoms.info['config_type']] = [es, fs]
            else:
                sigma_dict[atoms.info['config_type']] = [es]

    return sigma_dict


def plot_current_and_predicted_dimers(traj_in_name,model_path, model = 'GAP',dft_prefix='REF_',model_prefix='calc_', plot_isolated_atom_energy=True):
    """

    Plots dimer curve given my model - supports GAP or ACE or MACE

    Also plots the DFT dimers from the traj input. 

    Assuming length 2 atoms are isolated dimers and length 1 atoms are isolated atoms
    """



    distances = [0.5,0.6,0.7,0.8,0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9,5,5.1,5.2,5.3,5.4,5.5,5.6,5.7,5.8,5.9,6]

    traj = [Atoms('S2',cell=(34,34,34),positions=[[0,0,0],[i,0,0]],pbc=True) for i in distances]

    predicted_dimer_energies = []
    if model == 'GAP':
        from quippy.potential import Potential

        calc = Potential(param_filename=model_path)
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)
            
    elif model == 'ACE':
        import pyjulip
        calc = pyjulip.ACE1(model_path)
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)
            
    elif model == 'MACE':
        from mace.calculators import MACECalculator
        calc = MACECalculator(model_path=model_path,
                                r_max=5.0,
                                device='cuda',
                                atomic_numbers=[16],
                                default_dtype="float64")
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)

    traj_in = read(traj_in_name,':')

    dimer_dft = [atoms.info[f'{dft_prefix}energy']/2 for atoms in traj_in if len(atoms)==2]
    dimer_ml = [atoms.info[f'{model_prefix}energy']/2 for atoms in traj_in if len(atoms) == 2]
    dimer_distances = [atoms.get_distance(0,1) for atoms in traj_in if len(atoms)==2]

    if plot_isolated_atom_energy:
        iso_atom_dft = [atoms.info[f'{dft_prefix}energy'] for atoms in traj_in if len(atoms) == 1][0]
        iso_atom_ml = [atoms.info[f'{model_prefix}energy'] for atoms in traj_in if len(atoms)==1][0] 


    # print(dimer_dft)


    fig = plt.figure(figsize=(10,8))


    plt.plot(distances,predicted_dimer_energies,c='orange',label=f'{model} on dimers in and out of training.')
    plt.scatter(dimer_distances, dimer_dft,c='green', label='DFT on dimer training data')
    plt.scatter(dimer_distances, dimer_ml,c='cyan', label=f'{model} on dimer training data')


    if plot_isolated_atom_energy:
        plt.hlines(iso_atom_dft,xmin=0,xmax=6,color='green',label='Isolated atom DFT')
        plt.hlines(iso_atom_ml,xmin=0,xmax=6,ls='--',color='magenta',label=f'Isolated atom {model}')

    plt.xlabel('S2 bond length  (A)',fontsize=15)
    plt.ylabel('Energy (eV/atom)',fontsize=15)
    plt.title('Dimer energy predictions', fontsize=15)
    plt.legend(fontsize=14)

    return fig


def plot_predicted_dimer_curve(model_path, model = 'GAP', 
distances=[i/10 for i in range(5,60)]):
    """

    Plots dimer curve given my model - supports GAP or ACE or MACE

    Assuming length 2 atoms are isolated dimers and length 1 atoms are isolated atoms
    """

    traj = [Atoms('S2',cell=(34,34,34),positions=[[0,0,0],[i,0,0]],pbc=True) for i in distances]

    predicted_dimer_energies = []
    if model == 'GAP':
        from quippy.potential import Potential

        calc = Potential(param_filename=model_path)
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)
            
    elif model == 'ACE':
        import pyjulip
        calc = pyjulip.ACE1(model_path)
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)
            
    elif model == 'MACE':
        from mace.calculators import MACECalculator
        calc = MACECalculator(model_path=model_path,
                                r_max=5.0,
                                device='cuda',
                                atomic_numbers=[16],
                                default_dtype="float64")
        for atoms in traj:
            atoms.calc = calc
            predicted_dimer_energies.append(atoms.get_potential_energy()/2)

    fig = plt.figure(figsize=(10,8))

    plt.plot(distances,predicted_dimer_energies,c='orange',label=f'{model} on dimers in and out of training.')

    plt.xlabel('S2 bond length  (A)',fontsize=15)
    plt.ylabel('Energy (eV/atom)',fontsize=15)
    plt.title('Dimer energy predictions', fontsize=15)
    plt.legend(fontsize=14)
    plt.ylim(-304,-289)

    print(distances)
    print(predicted_dimer_energies)

    return fig


def cumulative_error_plot(trajs,labels, dft_prefix = 'REF_', model_prefix = 'calc_'):

    file_path = os.path.join(os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__))), 'model-GAP-6-test-GAP-6_teaching_database_force_error-properties.json')
        
    with open(file_path) as fl:
        results_dict = json.load(fl)

    errors_silicon = []
    for results in results_dict.values():
        for sub_result in results:
            errors_silicon.append(np.abs(sub_result[1]))

    if not isinstance(trajs, list):
        trajs = [trajs]
    if not isinstance(labels, list):
        labels = [labels]
    if not isinstance(model_prefix, list):
        model_prefix = [model_prefix]*len(trajs)
    if not isinstance(dft_prefix, list):
        dft_prefix = [dft_prefix]*len(trajs)


    errors_per_traj = []
    traj_idx = 0 
    for traj_name in trajs:
        traj = read(traj_name,':')
            
        error_per_atom = []

        for atoms in traj:
            if f'{dft_prefix[traj_idx]}forces' not in atoms.arrays:
                print(atoms, "does not have forces.")
            else:
                error = atoms.arrays[f'{dft_prefix[traj_idx]}forces'] - atoms.arrays[f'{model_prefix[traj_idx]}forces']
                error = error.flatten()
                error = np.abs(error)
        #         error = np.linalg.norm(error,axis=1)
                error_per_atom.extend(error)
        error_per_atom = np.array(error_per_atom)
        errors_per_traj.append(error_per_atom)
        
        traj_idx+=1

    fig = plt.figure(figsize=(9, 6))
    
    label_idx = 0
    for error_per_atom in errors_per_traj:
        # evaluate the histogram
        values, base = np.histogram(error_per_atom, bins=100000)
        #evaluate the cumulative
        cumulative = np.cumsum(values)
        cumulative = cumulative / np.max(cumulative)
        # plot the cumulative function
        plt.plot(base[:-1], cumulative, c=colors[label_idx],
                label=labels[label_idx])
        label_idx+=1


    # evaluate the histogram
    values, base = np.histogram(errors_silicon, bins=100000)
    #evaluate the cumulative
    cumulative = np.cumsum(values)
    cumulative = cumulative / np.max(cumulative)
    # plot the cumulative function
    plt.plot(base[:-1], cumulative, c='green', ls='--',
            label='Silicon PRX')

    plt.xscale('log')
    plt.tick_params(axis='both', which='major', labelsize=12)
    plt.xlabel('$\Delta$F (eV/A)', fontsize=13)
    plt.ylabel('p(error < $\Delta$F)', fontsize=13)
    plt.legend(fontsize=13)
    plt.grid()
    # plt.show()


def plot_energy_prediction_per_structure(model_paths, labels, models = 'GAP',traj_name='PBEsol_minimised_S_allotropes-FROM-Archer2-GoodPseudo-output-bareStructs-3rdConvergenceAttempt-MANUAL-output.xyz', use_internal=True,**kwargs):

    """
    If use_internal = True, will use the traj_name found in the script directory. 
    """

    if not isinstance(model_paths,list):
        model_paths = [model_paths]
    if not isinstance(labels,list):
        labels = [labels]
    if not isinstance(models,list):
        models = [models]

    if use_internal:
        traj_name = os.path.join(os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__))), traj_name)

    print(f'Using file {traj_name}.')
    dft_traj = read(traj_name, ':')


    config_types = [atoms.info['config_type'].split()[-1] for atoms in dft_traj]
    dft_energies_pa = [atoms.info['REF_energy'] / len(atoms) for atoms in dft_traj]

    fig = plt.figure(figsize=(10,6))
    plt.plot(dft_energies_pa, color=colors[0],label='DFT')




    label_idx = 0
    for model_path in model_paths:
        print(model_path)

        model = models[label_idx]
        model_energies = []
        if model == 'GAP':
            from quippy.potential import Potential
            calc = Potential(param_filename=model_path)
            for atoms in dft_traj:
                atoms.calc = calc
                model_energies.append(atoms.get_potential_energy() / len(atoms))
        elif model == 'ACE':
            import pyjulip
            calc = pyjulip.ACE1(model_path)
            for atoms in dft_traj:
                atoms.calc = calc
                model_energies.append(atoms.get_potential_energy() / len(atoms))
            
            
        plt.plot(model_energies, color=colors[label_idx+1],label=labels[label_idx])
        label_idx +=1

    ymin,ymax = np.min(dft_energies_pa)-0.01,np.max(dft_energies_pa)+0.04
    plt.ylim(bottom=ymin,top=ymax)


    # alpha_and_beta_S8_ener = [atoms.info['REF_energy'] / len(atoms) for atoms in dft_traj if 'S8' in atoms.info['config_type']]
    # energy_diff_S8 = abs(alpha_and_beta_S8_ener[1]-alpha_and_beta_S8_ener[0])
    
    plt.plot(
        [], [], ' ', label=f"Alpha S8 - Beta S8 DFT enthalpy difference (eV/at): {castep_enthalpy_between_alpha_and_beta_S8:.4f} \nAlpha S8 - Beta S8 exp. enthalpy difference (eV/at): {exp_enthalpy_between_alpha_and_beta_S8:.4f}")


    plt.xticks(range(8),labels=config_types, fontsize=12)
    plt.yticks(fontsize=12)
    plt.ylabel('Energy per atom (eV)', fontsize=13)
    plt.xlabel('Config', fontsize=13)
    plt.grid()
    plt.legend(fontsize=13)

    return fig



def plot_energy_prediction_per_structure2(file_names, models_prefixes =['ACE_'],labels=['ACE'], traj_name='PBEsol_minimised_S_allotropes-FROM-Archer2-GoodPseudo-output-bareStructs-3rdConvergenceAttempt-MANUAL-output.xyz', use_internal=True,**kwargs):

    """
    @param file_names is a (list of) xyz file(s) containing reference and model values for energy for the same structures
    If use_internal = True, will use the traj_name found in the script directory. 
    """

    if not isinstance(file_names,list):
        file_names = [file_names]
    if not isinstance(labels,list):
        labels = [labels]
    if not isinstance(models_prefixes,list):
        models_prefixes = [models_prefixes]

    if use_internal:
        traj_name = os.path.join(os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__))), traj_name)

    print(f'Using file {traj_name}.')
    dft_traj = read(traj_name, ':')


    config_types = [atoms.info['config_type'].split()[-1] for atoms in dft_traj]
    dft_energies_pa = [atoms.info['REF_energy'] / len(atoms) for atoms in dft_traj]

    fig = plt.figure(figsize=(10,6))
    plt.plot(dft_energies_pa, color=colors[0],label='DFT')




    label_idx = 0
    for file_name in file_names:
        print(file_name)

        model_traj = read(file_name,':')

        model_energies = [atoms.info[f'{models_prefixes[label_idx]}energy']/len(atoms) for atoms in model_traj]

        plt.plot(model_energies, color=colors[label_idx+1],label=labels[label_idx])
        label_idx +=1

    ymin,ymax = np.min(dft_energies_pa)-0.01,np.max(dft_energies_pa)+0.04
    plt.ylim(bottom=ymin,top=ymax)


    # alpha_and_beta_S8_ener = [atoms.info['REF_energy'] / len(atoms) for atoms in dft_traj if 'S8' in atoms.info['config_type']]
    # energy_diff_S8 = abs(alpha_and_beta_S8_ener[1]-alpha_and_beta_S8_ener[0])
    
    plt.plot(
        [], [], ' ', label=f"Alpha S8 - Beta S8 DFT enthalpy difference (eV/at): {castep_enthalpy_between_alpha_and_beta_S8:.4f} \nAlpha S8 - Beta S8 exp. enthalpy difference (eV/at): {exp_enthalpy_between_alpha_and_beta_S8:.4f}")


    plt.xticks(range(8),labels=config_types, fontsize=12)
    plt.yticks(fontsize=12)
    plt.ylabel('Energy per atom (eV)', fontsize=13)
    plt.xlabel('Config', fontsize=13)
    plt.grid()
    plt.legend(fontsize=13)

    return fig


def plot_energy_prediction_parity_plots(traj_name, gap_path, dft_prefix='REF_', **kwargs):

    from quippy.potential import Potential


    print(f'Using file {traj_name}.')
    dft_traj = read(traj_name, ':')
    
    dft_energies_pa = [atoms.info[f'{dft_prefix}energy'] / len(atoms) for atoms in dft_traj]

    print(gap_path)
    potential = Potential(param_filename=gap_path)
    gap_energies_pa = []
    for atoms in dft_traj:
        atoms.calc = potential
        gap_energies_pa.append(atoms.get_potential_energy() / len(atoms))


    # "Viridis-like" colormap with white background
    white_viridis = LinearSegmentedColormap.from_list('white_viridis', [
        (0, '#ffffff'),
        (1e-20, '#440053'),
        (0.2, '#404388'),
        (0.4, '#2a788e'),
        (0.6, '#21a784'),
        (0.8, '#78d151'),
        (1, '#fde624'),
    ], N=256)
    

    if 'density_plot' in kwargs:
        density_plot = kwargs['density_plot']

    if density_plot:
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 7), gridspec_kw={'hspace': 0.4},
                                subplot_kw={'projection': 'scatter_density'})  # squeeze false so they return a list even when theres only one element
    else:
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(
            6, 7), gridspec_kw={'hspace': 0.4})
        pass


    if density_plot:
        density = ax.scatter_density(dft_energies_pa,
                                               gap_energies_pa, cmap=white_viridis,
                                                dpi=30)
        fig.colorbar(
            density, label='Number of points per pixel', ax=ax)
    else:
        ax.scatter(dft_energies_pa,
                             gap_energies_pa)
        pass

    ax.set_xlabel('DFT energy (eV/atom)', fontsize=14)
    ax.set_ylabel('GAP energy (eV/atom)', fontsize=14)



    low_x, high_x = ax.get_xlim()
    low_y, high_y = ax.get_ylim()
    low = max(low_x, low_y)
    high = min(high_x, high_y)
    ax.set_xlim(low, high)
    ax.set_ylim(low, high)
    ax.plot([0, 1], [0, 1], ls='--', c='k',
                        transform=ax.transAxes)
                        
    return fig, ax


def plot_volume_scan_prediction(file_name, dft_prefix = 'REF_', model_prefix = 'calc_',label='model'):
    "file_name has to be a file which contains the REF and calc energies"


    test_traj = read(file_name, ':')

    traj_sorted = sorted(test_traj, key=lambda atoms: atoms.get_volume())
    volumes = [atoms.get_volume() / len(atoms) for atoms in traj_sorted]
    ref_energies = []
    gap_energies = []
    for atoms in traj_sorted:
        ref_energies.append(atoms.info[f'{dft_prefix}_energy'] / len(atoms))
        gap_energies.append(atoms.info[f'{model_prefix}_energy'] / len(atoms))

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(
        6, 7), gridspec_kw={'hspace': 0.4})


    ax.plot(volumes, ref_energies, label='DFT', ls='-', marker='o')
    ax.plot(volumes, gap_energies, label=label, ls='-', marker='o')

    ax.set_xlabel('Volume (A3/atom)', fontsize=14)
    ax.set_ylabel('Energy (eV/atom)', fontsize=14)

    ax.legend()
    ax.set_title('Volume scans', fontsize=14)

    return fig, ax


def plot_volume_scan_prediction_on_the_fly(gap_paths, labels, traj_name='22032022-2x2x1S8FromVolkerPaper-VariousDensities-Orthorhombic-RingsIntact-output-MANUAL.xyz', use_internal=True):

    """
    If use_internal = True, will use the traj_name found in the script directory. 
    """

    from quippy.potential import Potential

    if not isinstance(gap_paths,list):
        gap_paths = [gap_paths]
    if not isinstance(labels,list):
        labels = [labels]

    if use_internal:
        traj_name = os.path.join(os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__))), traj_name)

    print(f'Using file {traj_name}.')
    dft_traj = read(traj_name, ':')

    traj_sorted = sorted(dft_traj, key=lambda atoms: atoms.get_volume())
    volumes = [atoms.get_volume() / len(atoms) for atoms in traj_sorted]

    dft_energies_pa = [atoms.info['REF_energy'] / len(atoms) for atoms in traj_sorted]

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(
        10, 6), gridspec_kw={'hspace': 0.4})


    ax.plot(volumes, dft_energies_pa, color=colors[0], label='DFT', ls='-', marker='o')


    label_idx = 0
    for gap_path in gap_paths:
        print(gap_path)
        potential = Potential(param_filename=gap_path)
        gap_energies = []
        idx=0
        for atoms in traj_sorted:
            print(idx)
            idx+=1
            atoms.calc = potential
            gap_energies.append(atoms.get_potential_energy() / len(atoms))
        ax.plot(volumes, gap_energies, color=colors[label_idx+1],label=labels[label_idx])
        label_idx +=1



    ax.set_xlabel('Volume (A3/atom)', fontsize=14)
    ax.set_ylabel('Energy (eV/atom)', fontsize=14)

    ax.legend()
    ax.set_title('Volume scans', fontsize=14)

    return fig,ax 


def rms_dict(x_ref, x_pred):
    """ Takes two datasets of the same shape and returns a dictionary containing RMS error data"""
    """ from https://libatoms.github.io/GAP/gap_fitting_tutorial.html"""


    x_ref = np.array(x_ref)
    x_pred = np.array(x_pred)

    if np.shape(x_pred) != np.shape(x_ref):
        raise ValueError('WARNING: not matching shapes in rms')

    error_2 = (x_ref - x_pred) ** 2

    average = np.sqrt(np.average(error_2))
    std_ = np.sqrt(np.var(error_2))

    return {'rmse': average, 'std': std_}




def plot_inter_vs_intra(isolated_molecules,integral_structure,model_prefix='GAP_',dft_prefix='REF_',suptitle='specify suptitle - which iteration is this?', **kwargs):
    """
    given a structure containing molecules, and a set of structures containing the isolated molecules,
    this method plots the inter/intra forces and energies
    """

    intra_energies_dft = [atoms.info[f'{dft_prefix}energy']/len(atoms) for atoms in isolated_molecules] 
    intra_energies_ml = [atoms.info[f'{model_prefix}energy']/len(atoms) for atoms in isolated_molecules] 

    inter_energy_dft = (integral_structure.info[f'{dft_prefix}energy'] - sum([atoms.info[f'{dft_prefix}energy'] for atoms in isolated_molecules]))/len(integral_structure)
    inter_energy_ml = (integral_structure.info[f'{model_prefix}energy'] - sum([atoms.info[f'{model_prefix}energy'] for atoms in isolated_molecules]))/len(integral_structure)

    intra_forces_dft = np.array([atoms.arrays[f'{dft_prefix}forces'] for atoms in isolated_molecules]).flatten()
    intra_forces_ml = np.array([atoms.arrays[f'{model_prefix}forces'] for atoms in isolated_molecules]).flatten()

    inter_forces_dft = integral_structure.arrays[f'{dft_prefix}forces'].flatten() - np.array([atoms.arrays[f'{dft_prefix}forces'] for atoms in isolated_molecules]).flatten()
    inter_forces_ml = integral_structure.arrays[f'{model_prefix}forces'].flatten() - np.array([atoms.arrays[f'{model_prefix}forces'] for atoms in isolated_molecules]).flatten()

    integral_structure_forces_dft = integral_structure.arrays[f'{dft_prefix}forces']
    integral_structure_forces_ml = integral_structure.arrays[f'{model_prefix}forces']


    dpi=30
    if 'dpi' in kwargs.keys():
        dpi=kwargs['dpi']
    # "Viridis-like" colormap with white background
    white_viridis = LinearSegmentedColormap.from_list('white_viridis', [
        (0, '#ffffff'),
        (1e-20, '#440053'),
        (0.2, '#404388'),
        (0.4, '#2a788e'),
        (0.6, '#21a784'),
        (0.8, '#78d151'),
        (1, '#fde624'),
    ], N=256)


    if density_plot:
        fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(12, 15), gridspec_kw={'hspace': 0.4},
                                    subplot_kw={'projection': 'scatter_density'}) # squeeze false so they return a list even when theres only one element
    else:
        fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(12, 15), gridspec_kw={'hspace': 0.4})
    
    axs = axs.flatten()


    ax = axs[0]

    if density_plot:
        density = ax.scatter_density(intra_energies_dft,
                                        intra_energies_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(intra_energies_dft,
                                        intra_energies_ml, cmap=white_viridis)
    ax.set_xlabel('DFT energy (eV)', fontsize=14)
    ax.set_ylabel(f'{model_prefix.replace("_","")} energy (eV)', fontsize=14)
    ax.set_title('Intra energies per atom', fontsize=15)


    ax = axs[1]

    # density = ax.scatter_density(inter_energy_dft,
    #                                     inter_energy_ml, cmap=white_viridis,
    #                                     dpi=30)

    ax.text(0.5, 0.5, f'Inter energy DFT (eV) {inter_energy_dft} \nInter energy {model_prefix.replace("_","")} (eV) {inter_energy_ml}', horizontalalignment='center',
        verticalalignment='center', transform=ax.transAxes)

    # ax.set_xlabel('DFT energy (eV)', fontsize=14)
    # ax.set_ylabel('ml energy (eV)', fontsize=14)
    ax.set_title('Inter energy per atom', fontsize=15)
    fig.colorbar(density, label='Number of points per pixel', ax=ax)


    ax = axs[2]

    
    if density_plot:
        density = ax.scatter_density(inter_forces_dft,
                                        inter_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(inter_forces_dft,
                                        inter_forces_ml, cmap=white_viridis)

    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14)
    ax.set_title('Inter forces', fontsize=15)


    ax = axs[3]

    if density_plot:
        density = ax.scatter_density(intra_forces_dft,
                                        intra_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(intra_forces_dft,
                                        intra_forces_ml, cmap=white_viridis)
    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14)
    ax.set_title('Intra forces', fontsize=15)
    fig.colorbar(density, label='Number of points per pixel', ax=ax)



    ax = axs[4]

    if density_plot:
        density = ax.scatter_density(integral_structure_forces_dft,
                                        integral_structure_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(integral_structure_forces_dft,
                                        integral_structure_forces_ml, cmap=white_viridis)
    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14)
    ax.set_title('Overall forces', fontsize=15)
    fig.colorbar(density, label='Number of points per pixel', ax=ax)

    idx = 0
    for ax in axs:
        if idx == 1:
            idx+=1
            continue
        low_x, high_x = ax.get_xlim()
        low_y, high_y = ax.get_ylim()
        low = min(low_x, low_y)
        high = max(high_x, high_y)
        ax.set_xlim(low,high)
        ax.set_ylim(low,high)
        ax.plot([0, 1], [0, 1], ls='--', c='k',transform=ax.transAxes)
        idx+=1
        
    plt.suptitle(suptitle)


def plot_inter_vs_intra_2(isolated_molecules,integral_structure,model_prefix='GAP_',dft_prefix='REF_',suptitle='specify suptitle - which iteration is this?', **kwargs):
    """
    given a structure containing molecules, and a set of structures containing the isolated molecules,
    this method plots the inter/intra forces and energies
    """

    intra_energies_dft = [atoms.info[f'{dft_prefix}energy']/len(atoms) for atoms in isolated_molecules] 
    intra_energies_ml = [atoms.info[f'{model_prefix}energy']/len(atoms) for atoms in isolated_molecules] 

    inter_energy_dft = (integral_structure.info[f'{dft_prefix}energy'] - sum([atoms.info[f'{dft_prefix}energy'] for atoms in isolated_molecules]))/len(integral_structure)
    inter_energy_ml = (integral_structure.info[f'{model_prefix}energy'] - sum([atoms.info[f'{model_prefix}energy'] for atoms in isolated_molecules]))/len(integral_structure)

    intra_forces_dft = np.array([atoms.arrays[f'{dft_prefix}forces'] for atoms in isolated_molecules]).flatten()
    intra_forces_ml = np.array([atoms.arrays[f'{model_prefix}forces'] for atoms in isolated_molecules]).flatten()

    inter_forces_dft = integral_structure.arrays[f'{dft_prefix}forces'].flatten() - np.array([atoms.arrays[f'{dft_prefix}forces'] for atoms in isolated_molecules]).flatten()
    inter_forces_ml = integral_structure.arrays[f'{model_prefix}forces'].flatten() - np.array([atoms.arrays[f'{model_prefix}forces'] for atoms in isolated_molecules]).flatten()

    integral_structure_forces_dft = integral_structure.arrays[f'{dft_prefix}forces']
    integral_structure_forces_ml = integral_structure.arrays[f'{model_prefix}forces']

    dpi=30
    if 'dpi' in kwargs.keys():
        dpi=kwargs['dpi']

    # "Viridis-like" colormap with white background
    white_viridis = LinearSegmentedColormap.from_list('white_viridis', [
        (0, '#ffffff'),
        (1e-20, '#440053'),
        (0.2, '#404388'),
        (0.4, '#2a788e'),
        (0.6, '#21a784'),
        (0.8, '#78d151'),
        (1, '#fde624'),
    ], N=256)


    if density_plot:
        fig, axs = plt.subplots(nrows=1, ncols=4, figsize=(20, 5), gridspec_kw={'hspace': 2},
                                    subplot_kw={'projection': 'scatter_density'}) # squeeze false so they return a list even when theres only one element
    else:
        fig, axs = plt.subplots(nrows=1, ncols=4, figsize=(20, 5), gridspec_kw={'hspace': 2})
    
    axs = axs.flatten()


    ax = axs[0]

    if density_plot:
        density = ax.scatter_density(intra_energies_dft,
                                        intra_energies_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(intra_energies_dft,
                                        intra_energies_ml, cmap=white_viridis)
    ax.set_xlabel('DFT energy (eV)', fontsize=14)
    if model_prefix=='calc_':
        ax.set_ylabel('GAP energy (eV)', fontsize=14) # artefact
    else:
        ax.set_ylabel(f'{model_prefix.replace("_","")} energy (eV)', fontsize=14)
    ax.set_title('Intra energies per atom', fontsize=15)
    ax.tick_params(direction='inout')
    ax.xaxis.set_major_locator(MultipleLocator(0.02))

    # add text about RMSE
    _rms = rms_dict(intra_energies_dft, intra_energies_ml)
    rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV'
    ax.text(0.9, 0.1, rmse_text, transform=ax.transAxes, fontsize='large', horizontalalignment='right',
            verticalalignment='bottom')

    # # density = ax.scatter_density(inter_energy_dft,
    # #                                     inter_energy_ml, cmap=white_viridis,
    # #                                     dpi=30)

    # ax.text(0.5, 0.5, f'Inter energy DFT (eV) {inter_energy_dft} \nInter energy {model_prefix.replace("_","")} (eV) {inter_energy_ml}', horizontalalignment='center',
    #     verticalalignment='center', transform=ax.transAxes)

    # # ax.set_xlabel('DFT energy (eV)', fontsize=14)
    # # ax.set_ylabel('ml energy (eV)', fontsize=14)
    # ax.set_title('Inter energy per atom', fontsize=15)
    # fig.colorbar(density, label='Number of points per pixel', ax=ax)

    
    ax = axs[1]

    if density_plot:
        density = ax.scatter_density(inter_forces_dft,
                                        inter_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(inter_forces_dft,
                                        inter_forces_ml, cmap=white_viridis)

    ax.tick_params(labelleft=False,direction='in')
    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    if model_prefix=='calc_':
        ax.set_ylabel('GAP energy (eV)', fontsize=14) # artefact
    else:
        ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14,labelpad=5)
    ax.set_title('Inter forces', fontsize=15)

    # add text about RMSE
    _rms = rms_dict(inter_forces_dft, inter_forces_ml)
    rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV/Ang'
    ax.text(0.9, 0.1, rmse_text, transform=ax.transAxes, fontsize='large', horizontalalignment='right',
            verticalalignment='bottom')




    ax = axs[2]

    if density_plot:
        density = ax.scatter_density(intra_forces_dft,
                                        intra_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(intra_forces_dft,
                                        intra_forces_ml, cmap=white_viridis)
    ax.tick_params(labelleft=False,direction='in')
    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    # ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14)
    ax.set_title('Intra forces', fontsize=15)
    # fig.colorbar(density, label='Number of points per pixel', ax=ax)


    # add text about RMSE
    _rms = rms_dict(intra_forces_dft, intra_forces_ml)
    rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV/Ang'
    ax.text(0.9, 0.1, rmse_text, transform=ax.transAxes, fontsize='large', horizontalalignment='right',
            verticalalignment='bottom')



    ax = axs[3]

    if density_plot:
        density = ax.scatter_density(integral_structure_forces_dft,
                                        integral_structure_forces_ml, cmap=white_viridis,
                                        dpi=dpi)
    else:
        density = ax.scatter(integral_structure_forces_dft,
                                        integral_structure_forces_ml, cmap=white_viridis)
    ax.set_xlabel('DFT force component (eV/A)', fontsize=14)
    ax.tick_params(labelleft=False,direction='in')
    # ax.set_ylabel(f'{model_prefix.replace("_","")} force component (eV/A)', fontsize=14)
    ax.set_title('Overall forces', fontsize=15)
    # fig.colorbar(density, label='Number of points per pixel', ax=ax)


    # add text about RMSE
    _rms = rms_dict(integral_structure_forces_dft,  integral_structure_forces_ml)
    rmse_text = 'RMSE:\n' + str(np.round(_rms['rmse'], 3)) + 'eV/Ang'
    ax.text(0.9, 0.1, rmse_text, transform=ax.transAxes, fontsize='large', horizontalalignment='right',
            verticalalignment='bottom')


    idx = 0
    for ax in axs:
        low_x, high_x = ax.get_xlim()
        low_y, high_y = ax.get_ylim()
        low = min(low_x, low_y)
        high = max(high_x, high_y)
        ax.set_xlim(low,high)
        ax.set_ylim(low,high)
        ax.plot([0, 1], [0, 1], ls='--', c='k',transform=ax.transAxes)
        idx+=1

        
    plt.suptitle(suptitle)




def plot_toy_models(folder='./',polarised=True,dft_prefix='REF_',model_prefix='ACE_',legend_label='ACE'):
    """
    give it the folder where the polarised or the non polarised toy models live and it will do the rest
    """

    model_name = model_prefix.strip('_')
    calculations = {'non_polarised': {
    'Rings interaction':{'total':f'{folder}/28112022-rings_reaction-{model_name}-prediction.xyz',
    'isolated':f'{folder}/30112022-rings-reaction-isolated-molecules-output-MANUAL-{model_name}-prediction.xyz'},
    'Chains interaction':{'total':f'{folder}/28112022-chains_reaction-{model_name}-prediction.xyz',
    'isolated':f'{folder}/30112022-chains-reaction-isolated-molecules-output-MANUAL-{model_name}-prediction.xyz'},
    'Ring opening interaction':{'total':f'{folder}/27012023-ring-opening-reaction-rc3-output-MANUAL-{model_name}-prediction.xyz',
    'isolated':f'{folder}/20122022-ring-opening-reaction-output-MANUAL-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Along chain':{'isolated':f'{folder}/11022023-chains_reactions_RC3_MACEoptimised-toS2AlongChainLengthReaction_oneIsolatedChain-output-MANUAL-{model_name}-prediction.xyz',
    'total':f'{folder}/11022023-chains_reactions_RC3_MACEoptimised-toS2AlongChainLengthReaction-output-MANUAL-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Sideways':{'isolated':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_sideways_isolated-output-MANUAL-{model_name}-prediction.xyz',
    'total':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_sideways-output-MANUAL-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Away':{'total':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_away-output-MANUAL-{model_name}-prediction.xyz',
    'isolated':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_away_isolated-output-MANUAL-{model_name}-prediction.xyz'}
    },
    'polarised' :{
    'Rings interaction':{'total':f'{folder}/rings_reaction-bestOf-{model_name}-prediction.xyz',
    'isolated':f'{folder}/rings-reaction-isolated-molecules-bestOf-{model_name}-prediction.xyz'},
    'Chains interaction':{'total':f'{folder}/chains_reaction-bestOf-{model_name}-prediction.xyz',
    'isolated':f'{folder}/chains-reaction-isolated-molecules-bestOf-{model_name}-prediction.xyz'},
    'Ring opening interaction':{'total':f'{folder}/ring_opening_reaction_rc3-bestOf-{model_name}-prediction.xyz',
    'isolated':f'{folder}/ring_opening-bestOf-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Along chain':{'isolated':f'{folder}/chains_reactions_RC3_MACEoptimised-toS2AlongChainLengthReaction_oneIsolatedChain-bestOf-{model_name}-prediction.xyz',
    'total':f'{folder}/chains_reactions_RC3_MACEoptimised-toS2AlongChainLengthReaction-bestOf-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Sideways':{'isolated':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_sideways_isolated-bestOf-{model_name}-prediction.xyz',
    'total':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_sideways-bestOf-{model_name}-prediction.xyz'},
    'S8 chain into S2 - Away':{'total':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_away-bestOf-{model_name}-prediction.xyz',
    'isolated':f'{folder}/13022023optimised_chains_reactions_rc3_breaking_into_dimers_away_isolated-bestOf-{model_name}-prediction.xyz'}
    }}

    if polarised:
        config_types = calculations['polarised']
    else:
        config_types = calculations['non_polarised']

    def truncate_data(frame_numbers, array, n=16):
        truncated_frame_numbers=[]
        idx=0
        while idx<len(frame_numbers):
            truncated_frame_numbers.append(math.floor(idx))
            idx+=len(frame_numbers)/n
    #     print(truncated_frame_numbers,np.array(array)[truncated_frame_numbers])
        return truncated_frame_numbers,np.array(array)[truncated_frame_numbers]

    # Create a figure with 10 subplots arranged in 2 rows and 5 columns
    fig, axs = plt.subplots(6, 2, figsize=(19, 50))
    ylims =[(-0.6938436855, 1.0124858955),
    (-303.0230461855, -302.0667643645),
    (-0.822805918, 1.623388218),
    (-302.833191021, -301.25716049899995),
    (-1.5880407815, 0.509339515),
    (-302.8104000210334, -302.4128420924636),
    (-1.6887541074999999, 1.1663129975),
    (-302.89724320942065, -302.09231620953403),
    (-0.840181948, 0.3),
    (-302.89550276627983, -302.14633557589144),
    (-1.5457882945, 0.7009846845000001),
    (-302.898693213626, -302.0766602937631)]
    idx=0

    for ct,files in config_types.items():
        
        print(ct)
        total_energies_model = [atoms.info[f'{model_prefix}energy']/len(atoms) for atoms in read(files['total'],':')]
        total_energies_DFT = [atoms.info['REF_energy']/len(atoms) for atoms in read(files['total'],':')]
        isolated_energies_model = [atoms.info[f'{model_prefix}energy']/len(atoms) for atoms in read(files['isolated'],':')][:len(total_energies_DFT)] # doubled my isolated molecules for some reason
        isolated_energies_DFT = [atoms.info['REF_energy']/len(atoms) for atoms in read(files['isolated'],':')][:len(total_energies_DFT)] # doubled my isolated molecules for some reason
        interaction_energies_model = np.array(total_energies_model)-np.array(isolated_energies_model)
        interaction_energies_DFT = np.array(total_energies_DFT)-np.array(isolated_energies_DFT)
        
        ax = axs[idx][0]
        
        frame_numbers = range(len(isolated_energies_DFT))
        
        ax.set_ylabel('Interaction energy (eV/atom)', fontsize=20)
        ax.set_xlabel('Frame number',fontsize = 20)
        if ct == 'ring_':
            ct = 'ring_opening'
        ax.set_title(ct,fontsize=20)
        ax.tick_params(axis='both', which='major', labelsize=19)
        

        ax.plot(*truncate_data(frame_numbers,interaction_energies_DFT),c='blue',marker='')
        ax.plot(*truncate_data(frame_numbers,interaction_energies_model),c='blue',ls='',marker='^',markerfacecolor='none',markersize=14)

        
        ax = axs[idx][1]
        
        ax.set_ylabel('Total energy (eV/atom)', fontsize=20)
        ax.set_xlabel('Frame number',fontsize = 20)
        ax.tick_params(axis='both', which='major', labelsize=19)
        ax.set_title(ct,fontsize=20)
        axs[idx][0].set_ylim(ylims[idx*2])
        axs[idx][1].set_ylim(ylims[idx*2+1])
        
        ax.plot(*truncate_data(frame_numbers,total_energies_DFT),c='blue',marker='')
        ax.plot(*truncate_data(frame_numbers,total_energies_model),c='blue',ls='',marker='^',markerfacecolor='none',markersize=14)
        
        
        # Define custom lines for the legend
        dft_line = Line2D([0], [0], color='blue', linewidth=2, linestyle='-', label = f'{"Polarised" if polarised else "Non_polarised"} (DFT)')
        triangle_marker = Line2D([0], [0], marker='^', color='k', label=legend_label,
                markerfacecolor='none', markersize=10, ls='')
        
        if idx in [0,1]:
            legend = ax.legend(handles=[dft_line, triangle_marker],
            loc='upper right', bbox_to_anchor=(0.97, 0.97))
        else:
            legend = ax.legend(handles=[dft_line, triangle_marker],
            loc='lower right', bbox_to_anchor=(0.97, 0.03))
            
        for label in legend.get_texts():
            label.set_fontsize(19)   
        
        
        if idx in [0,1]:
            legend = axs[idx][0].legend(handles=[dft_line, triangle_marker],
            loc='upper right', bbox_to_anchor=(0.97, 0.97))
        else:
            legend = axs[idx][0].legend(handles=[dft_line, triangle_marker],
            loc='lower right', bbox_to_anchor=(0.97, 0.03))
            
        for label in legend.get_texts():
            label.set_fontsize(19)   
        
        idx+=1
    # Adjust the layout and spacing of the subplots
    fig.tight_layout(w_pad=3,h_pad=4)

    # Show the plot
    plt.show()
