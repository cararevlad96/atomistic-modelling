from ase.io import Trajectory




def format_traj_energies_to_violin(traj):
    '''
    Method used to re-format data which can be projected along one reaction coordinate to a format 
    amenable to violin plots.
    
    Pass a trajectory file whose ALL ATOMS have a consistent "distance" field, as in atoms.info["distance"].
    '''
    dist = []
    energies = []

    for atoms in traj: 
        dist.append(atoms.info['distance'])
        energies.append(atoms.get_potential_energy())

    energies = [x for _,x in sorted(zip(dist,energies))]
    dist.sort()


    print(len(dist)==len(energies))
    energies_violin = []

    aux_energies = []
    current_dist = dist[0]
    for counter in range(len(dist)):
        if(dist[counter]!=current_dist):
            energies_violin.append(aux_energies)
            current_dist = dist[counter]
            aux_energies=[]
        aux_energies.append(energies[counter])

    energies_violin.append(aux_energies) # to deal with the last distance 
    print(sorted(list(set(dist))))
    #print(len(sorted(list(set(dist)))))
    print(energies_violin)
    #print(len(energies_violin))
        