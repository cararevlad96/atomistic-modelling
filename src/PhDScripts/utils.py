"""
Small scripts for utils
"""

from ase.units import _amu

def get_density_in_gcm3(atoms):
    return atoms.get_masses().sum() * _amu * 1000 / \
        (atoms.get_volume() / (10**10)**3 * (10**2)**3)