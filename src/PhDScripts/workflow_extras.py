"""
Methods built on top of workflow
"""

import os, os.path
from ase.io.castep import read_castep_castep, read_castep_cell
from ase.io import read, write
import numpy as np
from ase import Atoms

def read_results_and_sort(og_traj,out_name, files):

    """
    helps read castep.castep files that workflow didn't put into an output file automatically

    make sure to not have isolated atoms in the traj
    """

    # og_traj = read('isolatedMolecules.xyz', ':')[1:]  # SKIP ISOLATED ATOM

    hashes_tuple_order = [('%.4f' % (atoms.get_positions()[0][0]), '%.4f' % (atoms.get_positions()[1][0])) for atoms in og_traj]
    print(hashes_tuple_order)

    if len(hashes_tuple_order) != len(set(hashes_tuple_order)):
        print('NON UNIQUE VALUES DETECTED')

    if os.path.exists(out_name):
        os.remove(out_name)

    tot_files = len(files)
    non_conv = 0

    traj_to_be_sorted = []
    idx_to_be_sorted = []
    idx = 0
    for file in files:
        try:
            with open(file) as fl:
                atoms = read_castep_castep(fl)[0]
                if 'initial_charges' in atoms.arrays:
                    del atoms.arrays['initial_charges']

                # if len(atoms) == 1:
                #     order_index = -1  # will increase all indeces by 1
                #     atoms_out = Atoms(atoms)

                # else:
                current_hash = ('%.4f' % (atoms.get_positions()[0][0]), '%.4f' % (
                    atoms.get_positions()[1][0]))

                order_index = hashes_tuple_order.index(current_hash)

                atoms_out = Atoms(og_traj[order_index])

                atoms_out.info['REF_energy'] = atoms.get_potential_energy()
                atoms_out.info['REF_stress'] = atoms.get_stress()
                atoms_out.arrays['REF_forces'] = atoms.get_forces()
                if 'REF_magnetic_moments' in atoms.arrays:
                    atoms_out.arrays['REF_magnetic_moments'] = atoms.get_initial_magnetic_moments(
                    )
                #             write(out,atoms,append=True)
        except AttributeError:
            cell_file = file.replace('.castep', '.cell')
            print(file)
            with open(cell_file) as fl:
                atoms = read_castep_cell(fl)
                current_hash = ('%.4f' % (atoms.get_positions()[0][0]), '%.4f' % (
                    atoms.get_positions()[1][0]))
                order_index = hashes_tuple_order.index(current_hash)
                atoms_out = Atoms(og_traj[order_index])

            non_conv += 1

        traj_to_be_sorted.append(atoms_out)
        idx_to_be_sorted.append(order_index)
        idx += 1

    # CUZ I PLACED THE ISOLATED ATOM MANUALLY
    # idx_to_be_sorted = np.array(idx_to_be_sorted) + 1

    sorted_traj = [atoms for idx, atoms in sorted(
        zip(idx_to_be_sorted, traj_to_be_sorted))]
    write(out_name, sorted_traj)

    print(str(tot_files) + " total calculations, ", str(non_conv) + " failed.")
